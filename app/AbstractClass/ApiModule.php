<?php
/**
 * Created by PhpStorm.
 * User: megidragon
 * Date: 9/5/19
 * Time: 8:10 AM
 */

namespace App\AbstractClass;

use App\Http\Controllers\Controller;
use App\Models\ExportQeue;
use App\Models\Params;
use App\Role\UserRole;
use Carbon\Carbon;
use Illuminate\Http\Request;

abstract class ApiModule extends Controller
{
    public static $_moduleClass;
    public static $_columns;
    public static $_rules;

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        // Trae todas las actividades segun los filtros pasados
        $results = $request->results ? $request->results : 20;
        $page = $request->page ? $request->page : 1;
        $orderBy = $request->orderBy ? $request->orderBy : in_array('datetime', static::$_columns) ? 'datetime' : 'id';
        $orderDir = $request->orderDir == 'ASC' ? 'ASC' : 'DESC';
        $condition = [];

        if (in_array('datetime', static::$_columns)) {
            $fromDate = $request->fromDate ? new Carbon(str_replace('T', ' ', $request->fromDate)) : Carbon::today();
            $toDate = $request->toDate ? new Carbon(str_replace('T', ' ', $request->toDate)) : Carbon::today()->addDays(5);
            $condition = [['datetime', '>=', $fromDate], ['datetime', '<=', $toDate]];
        }

        if (!auth()->user()->hasRole(UserRole::ROLE_SUB_ADMIN) && !auth()->user()->hasRole(UserRole::ROLE_ADMIN)) {
            $condition[] = [['user_id' => auth()->user()->id]];
        }


        $data = (new static::$_moduleClass)->connect(
            auth()->user()->active_cuit->business->db_name
        )
            ->columns()
            ->where($condition)
            ->skip(($page - 1) * $results)
            ->take($results)
            ->orderBy($orderBy, $orderDir)
            ->get();


        $total_results = (new static::$_moduleClass)->connect(auth()->user()->active_cuit->business->db_name)->where($condition)->count();
        if (!$data)
        {
            return response()->json([
                'success' => false,
                'error' => [
                    "error_code" => 400,
                    "error" => "BadRequest",
                    "msg" => "No se encontro la informacion solicitada."
                ]], 400);
        }

        $results = $data->toArray();


        return response()->json([
            'success' => true,
            "lastUpdate" => static::$_moduleClass::lastUpdate(),
            "results_count" => $data->count(),
            'total_results' => $total_results,
            'results' => $results,
            'app_version' => Params::getGlobalAppVersion()
        ], 200);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function verifyHash()
    {
        $response = [
            'success' => true,
            'lastUpdate' => (static::$_moduleClass)::lastUpdate(),
            'app_version' => Params::getGlobalAppVersion()
        ];
        return response()->json($response);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $data = (new static::$_moduleClass)->connect(auth()->user()->active_cuit->business->db_name)->columns()->where(['id' => $id])->get();

        if (!$data) {
            return response()->json([
                'success' => false,
                'error' => [
                    "error_code" => 400,
                    "error" => "BadRequest",
                    "msg" => "No se encontro la informacion solicitada."
                ]], 400);
        }

        return response()->json([
            'success' => true,
            'results' => $data->toArray(),
            "results_count" => $data->count(),
            'app_version' => Params::getGlobalAppVersion()
        ], 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->only(self::$_columns);

        $model = (new static::$_moduleClass)->connect(auth()->user()->active_cuit->business->db_name);

        foreach ($input as $key => $column)
        {
            $model->$key = $column;
        }
        $model->save();

        self::addQueueElement($model->id, $model->tactica_id, 'create');

        return response()->json(['success'=>true, 'inputs'=>$request->all()]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $input = $request->only(self::$_columns);

        try {
            $model = (new static::$_moduleClass)->connect(auth()->user()->active_cuit->business->db_name)->findOrFail($id);
        }catch (\Exception $e){
            return response()->json(['success'=>false, 'error'=>'record not found', 'inputs'=>$request->all()]);
        }

        foreach ($input as $key => $column)
        {
            $model->$key = $column;
        }
        $model->save();

        self::addQueueElement($id, $model->tactica_id, 'update');

        return response()->json(['success'=>true, 'inputs'=>$request->all()]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            $model = (new static::$_moduleClass)->connect(auth()->user()->active_cuit->business->db_name)->findOrFail($id);
        }catch (\Exception $e){
            return response()->json(['success'=>false, 'error'=>'record not found']);
        }

        self::addQueueElement(null, $model->tactica_id, 'delete');

        $model->delete();

        return response()->json(['success'=>true]);
    }

    private static function addQueueElement($ws_id, $tactica_id, $action)
    {
        $ws_table = (static::$_moduleClass)::getTableName();
        $tactica_table = (static::$_moduleClass)::getTacticaTableName();

        $eq = new ExportQeue();
        $eq->ws_table       = $ws_table;
        $eq->tactica_table  = $tactica_table;
        $eq->ws_id          = $ws_id;
        $eq->tactica_id     = $tactica_id;
        $eq->action         = $action;
        $eq->save();

        $business = auth()->user()->active_cuit->business;
        $business->last_update = date('Y-m-d H:i:s');
        $business->save();
    }
}