<?php

namespace App\AbstractClass;

use App\Models\Business;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;

abstract class ModuleModel extends Model
{
    abstract public static function getSyncSql(): string;

    abstract public static function getTacticaTableName(): string;

    abstract public static function onInsert($row): bool;

    abstract public static function onUpdate($row): bool;

    abstract public static function onDelete($row): bool;

    /**
     * @return string
     */
    public static function lastUpdate(): string{
        $business = auth()->user()->active_cuit->business;

        if (empty($business->last_update)){
            return 'empty';
        }

        return $business->last_update;
    }

    /**
     * @param $row
     * @return bool
     * @throws \Exception
     */
    public static function makeSync($row): bool{
        if ($row['operation_type'] == 'insert')
        {
            return static::onInsert($row);
        }
        elseif ($row['operation_type'] == 'update')
        {
            return static::onUpdate($row);
        }
        elseif ($row['operation_type'] == 'delete')
        {
            return static::onDelete($row);
        }
        else
        {
            throw new \Exception("Invalid operation_type");
        }

        return true;
    }

    /**
     * @param $data
     * @return bool
     */
    public static function dynamicInsertUpdate($data, Model $moduleInstance){
        $columns = (new static)->getColumns();
        // Cada columna dinamica con su valor dinamico
        foreach ($data as $column => $newValue) {
            if (!in_array($column, $columns)){
                continue;
            }
            $moduleInstance->$column = $newValue;
        }

        return $moduleInstance->save();
    }

    /**
     * @return array
     */
    public function getColumns(): array
    {
        return DB::getSchemaBuilder()->getColumnListing($this->table);
    }

    public static function getTableName(){
        return (new static)->table;
    }

    /**
     * @param $connName
     */
    public function setDatabase($connName){
        $this->connection = $connName;
    }

    /**
     * @param string $dbname
     * @return ModuleModel $this
     */
    public function connect($dbname){
        Config::set("database.connections.custom", [
            "host" => env('DB_HOST'),
            "database" => $dbname,
            "username" => env('DB_USERNAME'),
            "password" => env('DB_PASSWORD'),
            'driver' => 'mysql',
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'prefix_indexes' => true,
            'strict' => true,
            'engine' => null
        ]);

        $this->connection = 'custom';

        return $this;
    }

    public function getDate($key) {
        return (new Carbon($this->$key))->format('d/m/y');
    }
}