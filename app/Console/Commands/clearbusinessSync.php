<?php

namespace App\Console\Commands;

use App\Models\ActiveCuit;
use App\Models\Activities;
use App\Models\BusinessTactica;
use App\Models\User;
use Illuminate\Console\Command;

class clearbusinessSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'business:clear {cuit}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Limpia la base de datos de esa empresa por mala sincronizacion';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $business = ActiveCuit::query()->where('cuit', $this->argument('cuit'))->firstOrFail()->business;
        $this->info("Eliminando datos de {$business->empresa}");
        $modules = $business->getModuleKeys();

        if (in_array('activities', $modules)){
            $this->info("Eliminando actividades...");
            (new Activities)->connect($business->db_name)->where('id', '!=', 0)->delete();
        }

        if (in_array('business_tactica', $modules)) {
            $this->info("Eliminando empresas...");
            (new BusinessTactica)->where('id', '!=', 0)->delete();
        }

        $this->info("Eliminando usuario...");
        (new User)->where('email', '!=', 'admin@admin.com')->where(['cuit_id'=>$business->cuit_id])->delete();
        if ($business->db_name !== env('DB_DATABASE')) {
            (new User)->connect($business->db_name)->where('email', '!=', 'admin@admin.com')->delete();
        }

        $business->sync_installed = false;
        $business->save();

        $this->info("Completado, empresa {$business->empresa} limpia.");
    }
}
