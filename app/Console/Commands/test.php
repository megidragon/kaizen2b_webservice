<?php

namespace App\Console\Commands;

use App\Mail\ResetPassword;
use App\Models\BusinessTactica;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class test extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test command';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Mail::to('agustinh129@gmail.com')->send(new ResetPassword('asdasdasd'));
        $this->info('Enviado');
    }
}
