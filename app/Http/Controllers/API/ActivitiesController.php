<?php

namespace App\Http\Controllers\API;

use App\AbstractClass\ApiModule;
use App\Models\Activities;

class ActivitiesController extends ApiModule
{
    public static $_moduleClass = Activities::class;
    public static $_columns = [
        'id',
        'tactica_id',
        'name',
        'use_alert',
        'datetime',
        'datetime_alert',
        'description',
        'business',
        'contact',
        'priority',
        'start',
        'end',
        'type',
        'phone',
        'location',
        'user_id',
        'use_hour',
    ];
    public static $_rules = [];
}
