<?php

namespace App\Http\Controllers\Admin;

use App\Models\Auditory;
use App\Models\BusinessTactica;
use App\Models\Modules;
use App\Models\ServiceType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuditoryController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){
        $auditories = Auditory::query()->get();

        return view('admin.auditory_list', compact('auditories'));
    }


}
