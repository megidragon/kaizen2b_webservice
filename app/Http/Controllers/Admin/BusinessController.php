<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\PassportController;
use App\Models\ActiveCuit;
use App\Models\Auditory;
use App\Models\Business;
use App\Models\BusinessModules;
use App\Models\BusinessTactica;
use App\Models\Logs;
use App\Models\Modules;
use App\Models\ServiceType;
use App\Models\User;
use App\TableClass\ActivitiesCreator;
use App\TableClass\UsersCreator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use JsValidator;
use Mockery\Exception;

class BusinessController extends Controller
{
    const TOKEN_LENGHT = 42;

    protected $validator = [
        'service_type' => 'required',
        'modules' => 'required|array|min:1',
        'expire_date' => 'required|date',
        'cuit' => ''
    ];

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){
        $empresas_tactica = BusinessTactica::getBusiness();
        $validator = JsValidator::make($this->validator);
        $services = ServiceType::all();
        $modules = Modules::getModules();

        return view('admin.business_list', compact('empresas_tactica', 'validator', 'services', 'modules'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function active(){
        $empresas = Business::query()->with('business_module')->get();
        $modules = Modules::getModules();
        $services = ServiceType::all();
        $validator = JsValidator::make($this->validator);

        return view('admin.business_active_list', compact('empresas', 'validator', 'services', 'modules'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function down(Request $request, $id){
        $dbname = Business::find($id)->db_name;
        DB::transaction(function() use($id)
        {
            $business = Business::find($id);
            $business->business_module->each->delete();
            $business->users->each->delete();
            $business->active_cuit->delete();
            $business->delete();
        });

        $this->setDownDatabase($dbname);

        return response()->json(['success' => true]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function users($id){
        $business = Business::find($id);
        $usersActive = User::query()->where('password', '!=', null)->where(['cuit_id' => $business->cuit_id])->orderBy('email')->get();
        $usersInactive = User::query()->where('password', '=', null)->where('email', '!=', 'null')->where(['cuit_id' => $business->cuit_id])->orderBy('email')->get();

        return view('admin.business_users_list', compact('usersActive', 'usersInactive'));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function userDisable($id){
        if (empty($id) || $id == 'undefined'){
            return response()->json(['success' => false, 'msg' => 'id enviado no valido'], 500);
        }

        $user = User::query()->find($id);
        if (empty($user)) {
            Logs::log("User with id $id not found", 'error', null, 'admin-business-user-disable');
            return response()->json(['success' => false, 'msg' => 'No se encontro el usuario solicitado.'], 401);
        }

        $user->roles = null;
        $user->password = null;
        if (!$user->password && $user->status) {
            $user->status = false;
        }elseif(!$user->password && !$user->status){
            $user->status = true;
        }

        $user->save();

        return response()->json(['success' => true]);
    }

    public function show(){}

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function create(Request $request){
        $validator = Validator::make($request->all(), $this->validator);

        if ($validator->fails()) {
            return redirect(route('admin.business'))
                ->withErrors($validator)
                ->withInput();
        }
        $result = $this->setUpNewBusiness($request);

        if ($result !== false){
            // Log auditoria
            Auditory::logAction("Empresa \"{$result->empresa}\" dada de alta en el sistema", auth()->user()->id);

            return redirect(route('admin.business.active'));
        }
        else{
            Logs::log('Unknown error when trying to set up a business', 'fatal-error', null, 'admin-business-create');
            return redirect(route('admin.business'))
                ->withErrors($validator)
                ->withInput();
        }
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), $this->validator);

        if ($validator->fails()) {
            return redirect(route('admin.business.active'))
                ->withErrors($validator)
                ->withInput();
        }
    }


    /**
     * @param Request $request
     * @return Business|bool
     * @throws \Exception
     */
    private function setUpNewBusiness(Request $request){
        /*
         * Recibe los datos:
         * empresa directo de tactica
         * Duracion de licencia
         * Tipo de servicio
         * array con las tablas que sincronizara
         */
        $business = BusinessTactica::getBusiness($request->tactica_id);
        if (empty($business))
        {
            Logs::log('Business not found', 'error', null, 'admin-business-setup');
            return false;
        }
        elseif(!empty(Business::where(['id_tactica' => $business->tactica_id])->first()))
        {
            Logs::log('Business already exist', 'error', null, 'admin-business-setup');
            return false;
        }

        $current_timestamp = Carbon::now()->timestamp;
        $dbname = $current_timestamp.'_'.str_replace(' ','_',$business->empresa);
        $cuit = $business->document;

        // Valida la alta de la empresa
        $valid = $this->validateBusiness($business);
        if (!$valid)
        {
            Logs::log('Business not valid', 'error', null, 'admin-business-setup');
            return false;
        }

        $expire_date = \Carbon\Carbon::createFromFormat('m/d/Y', $request->expire_date);

        DB::beginTransaction();
        try
        {
            $active_ciut = $this->setActiveCuit($business, $cuit, $expire_date);
            $newBusiness = $this->createBusiness($business, $request, $dbname, $active_ciut);
            $this->setUpDatabase($dbname);
            $this->setUpTables($dbname);


            DB::commit();
            return $newBusiness;
        }catch (Exception $e){
            DB::rollBack();
            Logs::log($e->getMessage(), 'fatal-error', $e->getTraceAsString(), 'admin-business-setup');
            throw $e;
            return false;
        }
    }

    /**
     * @param $business
     * @param $request
     * @param $dbname
     * @param $cuit
     * @return Business
     * @throws ValidationException
     */
    private function createBusiness($business, $request, $dbname, ActiveCuit $active_cuit){
        $token = $this->generateClientToken();
        $new_business = new Business();
        $new_business->id_tactica = $business->tactica_id;
        $new_business->client_access_token = $token;
        $new_business->db_name = $dbname;
        $new_business->service_status = true;
        $new_business->cuit_id = $active_cuit->id;
        $new_business->service_key = $request->service_type;
        $new_business->sitio_web = $business->web_site;
        $new_business->contacts = $business->contacts;
        $new_business->calificacion = $business->qualification;
        $new_business->empresa = $business->empresa;
        $new_business->sync_installed = false;
        $new_business->sync_client_ip = null;

        // TODO: Falta añadir esta funcionalidad
        $new_business->use_custom_app_version = false;
        $new_business->app_version = null;

        $new_business->save();

        $new_business->assignModules($request->modules);

        // TODO: Ver si esto va, crea un primer usuario comun para probar la app
        self::createPrimaryUser($request, $active_cuit, $new_business);

        return $new_business;
    }

    private function validateBusiness($business){
        //TODO: Valida aca
        return true;
    }

    /**
     * @param $schemaName
     * @return bool
     */
    private function setUpDatabase($schemaName){
        return DB::statement('CREATE DATABASE '.$schemaName);
    }

    /**
     * @param $business
     * @param $cuit
     * @param $expire_date
     * @return ActiveCuit
     */
    private function setActiveCuit($business, $cuit, $expire_date){
        $ac = new ActiveCuit();
        $ac->tactica_id = $business->tactica_id;
        $ac->cuit = $cuit;
        $ac->name = $business->empresa;
        $ac->expire_date = $expire_date;
        $ac->save();

        return $ac;
    }

    private function migrateDataToDatabase($dbname){
        Config::set('database.connections.customconnection.host', env('DB_HOST'));
        Config::set('database.connections.customconnection.username', env('DB_USERNAME'));
        Config::set('database.connections.customconnection.password', env('DB_PASSWORD'));
        Config::set('database.connections.customconnection.database', $dbname);

        //TODO: Migra usuarios a la nueva db
        //TODO: Aca migra los datos
    }

    private function generateClientToken(){
        return self::generateRandomString(self::TOKEN_LENGHT);
    }

    public static function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    // TODO: Mejorar esto al rearmar el codigo, esto es estatico ahora
    private function setUpTables($dbname)
    {
        ActivitiesCreator::createTable($dbname);
        UsersCreator::createTable($dbname);
    }

    private function setDownDatabase($schemaName){
        return DB::statement('DROP DATABASE '.$schemaName);
    }

    /**
     * @param $request
     * @param $active_cuit
     * @param $new_business
     * @throws ValidationException
     */
    public static function createPrimaryUser(Request $request, $active_cuit, $new_business){
        $request->cuit = $active_cuit->id;
        $request->name = 'cliente01';
        $request->email = "test@".str_replace(' ', '-', $new_business->empresa).".com";
        $request->password = '12345';

        $response = (new PassportController)->register($request, true);
        if (!$response->success)
        {
            Logs::log($response->msg, 'fatal-error', null, 'admin-business-create');
            return false;
        }

        return true;
    }
}
