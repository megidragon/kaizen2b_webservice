<?php

namespace App\Http\Controllers\Admin;

use App\Models\ActiveCuit;
use App\Models\Auditory;
use App\Models\Business;
use App\Models\Logs;
use App\Models\Params;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class ConfigurationController extends Controller
{
    public function index()
    {
        $config = Params::getParams();
        $history = Params::getParamsHistory();
        return view('admin.config', compact('config', 'history'));
    }

    public function setAppVersion(Request $request){
        $validator = Validator::make($request->all(), [
            'global_app_version' => 'required|unique:params',
            'apk_app' => 'required|max:200000',
        ]);

        try {
            if ($validator->fails()){
                $errors = '';
                $i = 1;
                foreach ($validator->errors()->messages() as $error){
                    $errors .= $i > 1 ? ' - ' : '';
                    $errors .= "$i: {$error[0]}";
                    $i++;
                };

                Logs::log($errors, 'error', null, 'admin-configuration-set-app-version');
                throw new Exception($errors);
            }

            $storePath = public_path('files');
            if (!is_writable($storePath)){
                Logs::log("$storePath is not writable", 'fatal-error', null, 'admin-configuration-set-app-version');
                throw new Exception('Asegurese de tener los permisos del directorio files correctamente');
            }

            $file = $request->file('apk_app');
            $filename = $request->global_app_version.'.'.$file->getClientOriginalExtension();
            if (!Params::versionExists($request->global_app_version))
            {
                // Desavilita las viejas versiomes
                $params = Params::getParams();
                Params::cleanOldRows();

                $file->move($storePath, $filename);
                $params = $params->replicate();
                $params->global_app_version = $request->global_app_version;
                $params->app_description = $request->app_description;
                $params->current_version = true;
                $params->user_id = auth()->user()->id;
                $params->save();

                Auditory::logAction("Se subio una nueva version de la APP: {$params->global_app_version}", auth()->user()->id);
                $request->session()->flash('success', 'Version de la APP actualizada.');
            }else{
                $request->session()->flash('warning', 'No puede actualizar la misma version de la APP');
            }

        }catch (Exception $e)
        {
            Logs::log($e->getMessage(), 'error', $e->getTraceAsString(), 'admin-configuration-set-app-version');
            $request->session()->flash('error', $e->getMessage());
        }

        return redirect(route('admin.config'));
    }

    public function changeCurrentVersion(Request $request, $app_version)
    {
        $params = Params::query()->where(['global_app_version' => $app_version])->first();
        if ($params) {
            Params::cleanOldRows();
            $params->current_version = true;
            $params->user_id = auth()->user()->id;
            $params->save();
            $request->session()->flash('success', 'Version de la APP actualizada.');
        }else{
            $request->session()->flash('error', 'No puedo actualizar la version de la APP.');
        }

        return redirect(route('admin.config'));
    }
}
