<?php

namespace App\Http\Controllers\Admin;

use App\Models\ActiveCuit;
use App\Models\Business;
use App\Models\Params;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $businessCount = Business::query()->where(['service_status' => 1])->count();
        $businessSyncCount = Business::query()->where(['service_status' => 1, 'sync_installed' => true])->count();
        $businessPercentage = $businessSyncCount && $businessCount ? round($businessSyncCount * 100 / $businessCount, 2) : 0;

        $acActive = ActiveCuit::query()->where('expire_date', '>=', DB::raw('NOW()'))->count();
        $acTotal = ActiveCuit::query()->whereHas('business', function($q){ return $q->where('service_status', '=', 1); })->count();
        $acPercentage = $acActive && $acTotal ? round($acActive * 100 / $acTotal, 2) : 0;
        $config = Params::getParams();

        return view('admin.dashboard', compact('businessCount', 'businessSyncCount', 'businessPercentage', 'acActive', 'acTotal', 'acPercentage', 'config'));
    }
}
