<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\PassportController;
use App\Models\Auditory;
use App\Models\Logs;
use App\Models\User;
use App\Role\UserRole;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use JsValidator;
use Mockery\Exception;
use Illuminate\Validation\Rule;


class UsersController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){
        $users = User::query()->whereJsonContains('roles', UserRole::ROLE_SUB_ADMIN)->get();

        return view('admin.users.users_list', compact('users'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request, $id)
    {
        $user = User::query()->where(['id' => $id])->whereJsonContains('roles', UserRole::ROLE_SUB_ADMIN)->first();

        return view('admin.users.user_show', compact('user'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:255',
            'email' => 'required|email|unique:users|max:255',
            'password' => 'required|confirmed|min:6',
        ];
        try {
            $validator = JsValidator::make($rules);
            if ($request->post()) {
                $validation = Validator::make($request->all(), $rules);
                if ($validation->fails()) {
                    $request->session()->flash('error', 'Se encontro un error al procesar su solicitud');
                    return redirect(url()->previous());
                }

                $request->cuit = auth()->user()->cuit_id;

                $response = (new PassportController)->register($request, true, UserRole::ROLE_SUB_ADMIN);
                if ($response->success) {
                    $request->session()->flash('success', 'Usuario creado con exito');
                    // Log auditoria
                    Auditory::logAction("Nuevo usuario administrador \"{$request->email}\" dado de alta", auth()->user()->id);
                    return redirect(route('admin.users'));
                }
                $request->session()->flash('error', 'Hubo un error en la creacion del usuario.');
                Logs::log($response->msg, 'error', null, 'admin-user-store');
            }

            return view('admin.users.user_form', compact('validator'));
        }catch (Exception $e)
        {
            Logs::log($e->getMessage(), 'fatal-error', $e->getTraceAsString(), 'admin-user-store');
            throw $e;
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(Request $request, $id)
    {
        $user = User::query()->where(['id' => $id])->whereJsonContains('roles', UserRole::ROLE_SUB_ADMIN)->first();
        if(empty($user)){
            Logs::log('User doesn\'t exist', 'warning', '', 'admin-user-update');
            throw new Exception('Usuario inexistente');
        }

        $rules = [
            'name' => 'required|max:255',
            'email' => [
                "required",
                "email",
                "max:255",
                Rule::unique('users')->ignore($user->id),
            ],
            'password' => 'nullable|confirmed|min:6',
        ];
        $validator = JsValidator::make($rules);

        if ($request->post())
        {
            try {
                $validation = Validator::make($request->all(), $rules);
                if ($validation->fails()) {
                    $request->session()->flash('error', 'Se encontro un error al procesar su solicitud');
                    return redirect(url()->previous());
                }
                $user->name = $request->name;
                $user->email = $request->email;
                $user->status = $request->status == 1 ? true : false;
                if (!empty($request->password)) {
                    $user->password = $request->password;
                }
                $user->save();

                $request->session()->flash('success', 'Usuario modificado con exito');
                // Log auditoria
                Auditory::logAction("Se realizaron modificaciones en usuario administrador \"{$user->email}\"", auth()->user()->id);
                return redirect(route('admin.users'));
            }catch (Exception $e){
                $request->session()->flash('error', 'Hubo un problema al modificar el usuario.');
                Logs::log($e->getMessage(), 'error', $e->getTraceAsString(), 'admin-user-update');
            }
        }

        return view('admin.users.user_form', compact('user', 'validator'));
    }

    /**
     * @param Request $request
     * @param $id integer
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function delete(Request $request, $id)
    {
        try {
            $user = User::query()->where(['id' => $id])->whereJsonContains('roles', UserRole::ROLE_SUB_ADMIN)->first();
            $oldMail = $user->email;
            $user->delete();
        }catch (Exception $e){
            Logs::log($e->getMessage(), 'error', $e->getTraceAsString(), 'admin-user-delete');
            return response()->json(['success' => false, 'msg'=>$e->getMessage()], 500);
        }

        // Log auditoria
        Auditory::logAction("Usuario administrador \"{$oldMail}\" eliminado", auth()->user()->id);

        return response()->json(['success' => true, 'msg' => 'usuario eliminado con exito.'], 200);
    }

}
