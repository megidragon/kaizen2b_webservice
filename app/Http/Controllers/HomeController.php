<?php

namespace App\Http\Controllers;

use App\Models\Logs;
use Illuminate\Http\Request;
use GuzzleHttp;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('index');
    }

    public function activities()
    {
        return view('activities');
    }

    public function downloadApk(Request $request, $version){
        $file = public_path("/files/$version.apk");

        if (!file_exists($file)){
            return response()->json(['success' => false, 'msg' => 'El archivo solicitado no se encuentra accesible.']);
        }

        return response()->file($file ,[
            'Content-Type'=>'application/vnd.android.package-archive',
            'Content-Disposition'=> 'attachment; filename="'.$version.'.apk"',
        ]) ;
    }

    public function downloadSync(Request $request){
        $file = public_path("/sync/kzn_sync.zip");

        if (!file_exists($file)){
            return response()->json(['success' => false, 'msg' => 'El archivo solicitado no se encuentra accesible.']);
        }

        return response()->file($file ,[
            'Content-Type'=>'application/zip',
            'Content-Transfer-Encoding'=>'Binary',
            'Content-Length'=> filesize($file),
            'Content-Disposition'=> 'attachment; filename="kzn_sync.zip"',
        ]) ;
    }

    public function logInsert(Request $request){
        if (empty($request->logs)){
            return response()->json([
                'success' => false,
                'msg' => 'No se encontraron logs'
            ]);
        }

        foreach ($request->logs as $log) {
            $exeption = $log['exeption'];
            $description = $log['description'];
            $tag = $log['tag'];
            $type = $log['type'];
            $time = $log['time'];

            Logs::log($exeption, $type, $description, $tag, $time, auth()->user()->id);
        }

        return response()->json([
            'success' => true
        ]);
    }
}
