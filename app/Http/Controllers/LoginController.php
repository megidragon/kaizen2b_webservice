<?php

namespace App\Http\Controllers;

use App\Mail\ResetPassword;
use App\Models\PasswordResets;
use App\Models\User;
use Illuminate\Http\Request;
use GuzzleHttp;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Lunaweb\RecaptchaV3\Facades\RecaptchaV3;

class LoginController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function login()
    {
        if (!Auth::guest()) {
            return redirect('activities');
        }
        return view('login');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function loginWs(Request $request){
        $valid = Validator::make(Input::all(), [
            'g-recaptcha-response' => 'required|recaptchav3:login,0.5',
            'cuit' => '',
            'email' => 'required|email',
            'password' => '',
        ]);

        $score = RecaptchaV3::verify($request->get('g-recaptcha-response'), 'login');

        if ($score > 0.7){
            $response = json_decode((new PassportController)->login($request)->content());
            if ($response->success && !$response->new_user)
            {
                $request->session()->put('token', $response->token);
                return response()->json($response, 200);
            }elseif (!empty($response->new_user)){
                return response()->json($response, 200);
            }
        }
        elseif($score > 0.1)
        {
            $response = json_decode((new PassportController)->login($request)->content());
            if ($response->success && !$response->new_user)
            {
                $request->session()->put('token', $response->token);
                return response()->json($response, 200);
            }elseif (!empty($response->new_user)){
                return response()->json($response, 200);
            }
        }
        else
        {
            return response()->json([
                'success' => false,
                'error' => [
                    "error_code" => 401,
                    "error" => 'UnAuthenticated',
                    "msg" => 'Se ha detectado problemas al generar sesion, pruebe con otro navegador.'
                ]
            ], 401);
        }

        return response()->json([
            'success' => false,
            'error' => [
                "error_code" => 401,
                "error" => 'UnAuthenticated',
                "msg" => 'Error al generar la sesion, credenciales invalidas.'
            ]
        ], 401);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function logOut(){
        (new PassportController)->logout();
        auth()->logout();
        return redirect(route('login'));
    }

    public function forgotPassword(){
        return view('forgot-password');
    }

    public function sendForgotPasswordMail(Request $request){
        $user = User::query()->where(['email' => $request->email])->first();
        if ($user)
        {
            if ($user->active_cuit->cuit !== $request->cuit)
            {
                $request->session()->flash('error', 'El cuit no pertenece a ninguna empresa activa.');
                return redirect(route('forgot-password'));
            }

            // Si pasa las validaciones lo envia

            // Genera nuevo token
            $token = $this->createResetToken($user);
            Mail::to($user->email)->send(new ResetPassword($token));

            $request->session()->flash('success', 'Se envio un link de confirmacion para restablecer su contraseña.');
        }
        else
        {
            $request->session()->flash('error', 'No se encuentra ningun usuario con ese email.');
        }

        return redirect(route('forgot-password'));
    }

    public function resetPassword(Request $request, $token){
        $pr = PasswordResets::query()->where(['token' => $token])->first();
        if (!empty($pr))
        {
            $start_date = new \DateTime();
            $start_date = $start_date->diff(new \DateTime($pr->created_at));
            if ($start_date->i <= 15) {
                $user = User::query()->where('email', $pr->getOriginal('email'))->firstOrFail();
                $user->password = null;
                $user->save();

                // Si pasa se elimina el pr
                // TODO: DEBUG
                $pr->delete();
                $request->session()->flash('success', 'Su contraseña se ha restablecido con exito, inicie sesion para crear una nueva.');
                return redirect(route('login'));
            }

            $pr->delete();
        }

        $request->session()->flash('error', 'Token expirado');
        return redirect(route('login'));
    }

    private function createResetToken($user){
        $token = sha1(date('YMDHIS'));
        $pr = new PasswordResets;
        $pr->email = $user->email;
        $pr->token = $token;
        $pr->save();
        return $token;

    }
}
