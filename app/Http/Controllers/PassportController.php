<?php

namespace App\Http\Controllers;

use App\Models\ActiveCuit;
use App\Models\Business;
use App\Models\Params;
use App\Models\User;
use App\Models\UserConnections;
use App\Role\UserRole;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class PassportController extends Controller
{
    /**
     * Handles Registration Request
     *
     * @param Request $request, $withoutParse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function register(Request $request, $withoutParse=false, $role=UserRole::ROLE_CLIENT)
    {
        /*if (!$withoutParse) {
            $this->validate($request, [
                'cuit' => 'min:8',
                'name' => 'required|min:3',
                'email' => 'required|email', // |unique:users
                'password' => 'required|min:5',
            ]);
        }*/
        $insert = [
            'cuit_id' => $request->cuit,
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ];
        DB::beginTransaction();
        try {
            $user = User::create($insert);

            $user->addRole($role)->save();

            $token = $user->createToken('custom_token')->accessToken;

            /*ActiveCuit::create([
                'cuit' => $request->cuit,
                'tactica_id' => '-',
                'name' => '-',
                'expire_date' => Carbon::now()->addMonths(2)
            ]);*/
            $response = ['success' => true, 'token' => 'Bearer ' . $token];

            DB::commit();
            if ($withoutParse) {
                return (object)$response;
            }
            return response()->json($response, 200);
        }catch (Exception $e)
        {
            DB::rollBack();
            return response()->json(['success' => false, 'msg' => $e->getMessage()], 500);
        }
    }

    /**
     * @param Request $request
     * @param $cuit
     * @param $email
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function newUser(Request $request, $cuit, $email)
    {
        $user = User::where(['cuit_id' => $cuit, 'email' => $email])->first();
        if (empty($user))
        {
            return response()->json([
                'success' => false,
                'msg' => 'Usuario inexistente'
            ], 401);
        }

        $user->password = bcrypt($request->new_password);

        $user->addRole(UserRole::ROLE_CLIENT)->save();

        return response()->json([
            'success' => true,
            'msg' => 'Usuario dado de alta correctamente, inicie sesion para validar usuario'
        ], 200);
    }

    /**
     * Handles Login Request
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request, $masterpass=false)
    {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password,
            'status' => true
        ];

        // TODO: Si es la primera vez q se logea crea usuario en la db principal
        // TODO: Si ya existe saca el hash del usuario en la tabla original de tactica y compara con el guardado en la tabla users
        $msg = null;
        $error = null;
        try {
            // Si existe el cuit de la empresa en la db
            if (empty($request->cuit))
            {
                return response()->json([
                    'success' => false,
                    'error' => [
                        "error_code" => 401,
                        "error" => 'cuit field is required',
                        "msg" => 'Cuit requerido'
                    ]
                ], 401);
            }

            $cuit = ActiveCuit::where(['cuit' => $request->cuit])->first();

            if(!empty($cuit)) {
                $app_version = Params::getGlobalAppVersion();
                $credentials['cuit_id'] = $cuit->id;
                $conditions = [
                    'email' => $credentials['email'],
                    'status' => true
                ];
                $conditions['cuit_id'] = $credentials['cuit_id'];

                // Master pass
                if ($masterpass) {
                    Auth::loginUsingId(User::query()->where($conditions)->first()->id);
                    if (
                        auth()->user()->last_token && 
                        auth()->user()->oauth_acess_token->first()->revoked === 0 && 
                        auth()->user()->oauth_acess_token->first()->expires_at > date('Y-m-d H:i:s'))
                    {
                        $token = auth()->user()->last_token;
                    }else {
                        $user = auth()->user();
                        // Si intenta logearse y no tiene guardado el ultimo token elimina todos y crea uno nuevo, para evitar conflictos
                        $user->oauth_acess_token()->delete();
                        $user->last_token = null;
                        $user->save();

                        $token = auth()->user()->createToken(auth()->user()->email)->accessToken;
                        $user->last_token = $token;
                        $user->save();
                    }
                    return $this->successResponse($request, $token);
                } elseif (auth()->attempt($credentials)) {
                    // Si el usuario existe valida cuit
                    $date = new Carbon;
                    if (auth()->user()->active_cuit->expire_date >= $date) {
                        if (auth()->user()->last_token && auth()->user()->oauth_acess_token->first()->revoked === 0 && auth()->user()->oauth_acess_token->first()->expires_at > date('Y-m-d H:i:s')){
                            $token = auth()->user()->last_token;
                        }else {
                            $user = auth()->user();
                            // Si intenta logearse y no tiene guardado el ultimo token elimina todos y crea uno nuevo, para evitar conflictos
                            $user->oauth_acess_token()->delete();
                            $user->last_token = null;
                            $user->save();

                            $token = auth()->user()->createToken(auth()->user()->email)->accessToken;
                            $user->last_token = $token;
                            $user->save();
                        }
                        $this->log_connection($request);

                        return $this->successResponse($request, $token);
                    } else {
                        $msg = "Cuit no valido.";
                        $error = "ExpiredCuit";
                    }
                }
                elseif ($user = User::where($conditions)->where(['password' => null])->first()) {
                    return response()->json([
                        'success' => true,
                        'new_user' => true,
                        'new_user_setup' => url('/api/new-user/' . $cuit->id . '/' . $user->email),
                        'app_version' => $app_version,
                    ], 200);
                } else {
                    $msg = "Usuario no valido.";
                    $error = "UnAuthorized";
                }
            }else{
                $error = 'active cuit not found';
                $msg = 'El cuit solicitado no se encuentra dado de alta en el sistema';
            }

            return response()->json([
                'success' => false,
                'error' => [
                    "error_code" => 401,
                    "error" => $error,
                    "msg" => $msg
                ]
            ], 401);
        }catch (Exception $e){
            return response()->json([
                'success' => false,
                'error' => [
                    "error_code" => 500,
                    "error" => 'Error interno del sistema',
                    "msg" => $e->getMessage()
                ]
            ], 401);
        }
    }

    /**
     * @return bool
     */
    public function logout(){
        return response()->json([
            'success' => true,
        ], 200);

        if (auth()->check()) {
            $user = auth()->user();
            $user->oauth_acess_token()->delete();
            $user->last_token = null;
            $user->save();

            return response()->json([
                'success' => true,
            ], 200);
        }

        return response()->json([
            'success' => false,
            'msg' => 'Error al ejecutar esta accion'
        ], 500);
    }

    /**
     * Returns Authenticated User Details
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function details()
    {
        try {
            $user_data = auth()->user();
            auth()->user()->active_cuit->business->select();

            return response()->json(['success' => true, 'user' => $user_data], 200);
        }catch(\Exception $e){
            return response()->json(['success' => false, 'error' => [
                "error_code" => 401,
                "error" => $e->getMessage(),
                "msg" => $e->getMessage()
            ]], 401);
        }
    }

    /**
     * @param Request $request
     * return void
     */
    private function log_connection(Request $request){
        // Trae id de empresa
        $business = Business::where(['cuit_id' => auth()->user()->active_cuit->id])->first();
        $business_id = $business ? $business->id : null;

        $imei = !empty($request->imei) ? $request->imei : null;

        // Busca si ya esta el log del usuario por id de usuario y imei
        $user_log = UserConnections::where(['user_id' => auth()->user()->id, 'imei' => $imei])->first();
        if (empty($user_log)) {
            $user_log = new UserConnections();
        }

        $user_log->business_id = $business_id;
        $user_log->user_id = auth()->user()->id;
        $user_log->ip = $request->ip();
        $user_log->device_info = !empty($request->device_info) ? $request->device_info : null;
        $user_log->imei = $imei;
        $user_log->last_log = $user_log->imei;
        $user_log->save();

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    private function successResponse(Request $request, $token)
    {
        $this->log_connection($request);

        return response()->json([
            'success' => true,
            'new_user' => false,
            'token' => 'Bearer ' . $token,
            'user_role' => auth()->user()->roles,
            'app_version' => Params::getGlobalAppVersion(),
        ], 200);
    }
}
