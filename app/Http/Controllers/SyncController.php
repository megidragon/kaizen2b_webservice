<?php

namespace App\Http\Controllers;

use App\Models;
use App\Models\Auditory;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class SyncController extends Controller
{
    /*
     * Aca hay q dar de alta los modulos con los servicios ya creados (Despues del refactor de codigo)
     */
    private $Modules = [
        'activities' => Models\Activities::class,
        'business_tactica' => Models\BusinessTactica::class,
        'user' => Models\User::class,
    ];

    public function serviceStatus(Request $request){
        $service_status = true;
        $service_status = $service_status ? auth()->user()->active_cuit->business->service_status == true : false;
        $service_status = $service_status ? auth()->user()->active_cuit->expire_date > date('Y-m-d H:i:s') : false;


        $response = [
            'success' => true,
            'service_status' => $service_status,
        ];
        return response()->json($response, 200);
    }

    public function info(Request $request)
    {
        try {
            $activeCuit = auth()->user()->active_cuit;
            $service_status = true;
            $service_status = $service_status ? auth()->user()->active_cuit->business->service_status == true : false;
            $service_status = $service_status ? auth()->user()->active_cuit->expire_date > date('Y-m-d H:i:s') : false;

            $modules = $activeCuit->business->getModuleKeys(true);
            $order = array_merge(['user'], $modules);
            $modules[] = 'user';

            $moduleProcessFormat = [];
            foreach ($this->Modules as $key => $module) {
                if (!in_array($key, $modules)) {
                    continue;
                }
                $columns = (new $module)->getColumns();
                $sql = $module::getSyncSql();
                $tacticaTableName = $module::getTacticaTableName();
                $moduleProcessFormat[$tacticaTableName] = [
                    'columns' => $columns,
                    'sql' => $sql,
                    'ws_table_name' => $key
                ];

            }

            $response = [
                'success' => $service_status,
                'modules' => $modules,
                'order' => $order,
                'expiration_date' => $activeCuit->expire_date,
                'sync_installed' => $activeCuit->business->sync_installed,
                'db_name' => $activeCuit->business->db_name,
                'service_status' => $service_status,
                'modules_process_format' => $moduleProcessFormat,
            ];

            return response()->json($response, 200);
        }catch (\Exception $e) {
            return response()->json(['success' => false, 'msg'=>$e->getMessage()], 500);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function sync(Request $request)
    {
        /*
         * Formato de entrada sera json
         * {
         *      'sync_installed' : true|false (optional)
         *      'modules': [
         *          'activities': [
         *              {
         *                  'operation_type': 'insert' / 'update'
         *                  'data': {
         *                      // ALL COLUMNS OF THIS ROW //
         *                  }
         *              }
         *          ]
         *      ]
         * }
         */

        if (empty($request->modules)){
            return response()->json(['success' => false,
                'error' => [
                    "error_code" => 200,
                    "error" => "EmptyModules",
                    "msg" => ""
                ]], 200);
        }

        try
        {
            // Sincroniza usuarios con la db principal
            if (!empty($request->modules['user'])){
                foreach ($request->modules['user'] as $row) {
                    Models\User::makeSync($row);
                }
            }

            // Modulos
            foreach ($request->modules as $key => $module)
            {
                // Valida q tenga permiso al modulo (no aplica al modulo usuarios)
                $modules_keys = auth()->user()->active_cuit->business->getModuleKeys();

                if ($key == 'user' || !in_array($key, $modules_keys, true))
                {
                    continue;
                }

                // Por cada modulo las rows
                foreach($module as $row)
                {
                    // Si el modulo no esta registrado exepciona
                    if (!isset($this->Modules[$key]) || !class_exists($this->Modules[$key]))
                    {
                        throw new Exception("class $key not found");
                    }
                    (new $this->Modules[$key])::makeSync($row);
                }
            }

            // Si sincroniza algun cambio lo deja registrado en last update de la empresa
            $business = auth()->user()->active_cuit->business;
            $business->last_update = date('Y-m-d H:i:s');
            $business->save();
        }
        catch (Exception $e)
        {
            Models\Logs::log($e->getMessage(), 'error', $e->getTraceAsString(), 'sync', null);
            return response()->json(['success' => false, 'error' => [
                "error_code" => 500,
                "error" => "Catch error",
                "msg" => $e->getMessage()
            ]], 500);
        }

        if (!empty($request->sync_installed))
        {
            $business = auth()->user()->active_cuit->business;
            $business->sync_installed = true;
            $business->sync_client_ip = $request->ip();
            $business->save();
            Auditory::logAction("Empresa \"{$business->empresa}\" realizo su primera sincronizacion de datos", auth()->user()->id);
        }

        return response()->json(['success'=>true, 'error' => []]);
    }
}
