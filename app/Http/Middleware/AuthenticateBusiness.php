<?php

namespace App\Http\Middleware;

use App\Http\Controllers\Admin\BusinessController;
use App\Http\Controllers\PassportController;
use App\Models\Business;
use Closure;

class AuthenticateBusiness
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function handle($request, Closure $next)
    {
        // TODO: Aca restringe el acceso y deja pasar unicamente cuando se envia el hash por header q sea valido
        $business = Business::query()->where(['client_access_token' => $request->header('AuthorizationBusiness')])->first();

        if (empty($business))
        {
            return response()->json(["message" => "Unauthenticated."], 401);
        }

        if ($business->active_cuit->users->count() === 0){
            BusinessController::createPrimaryUser($request, $business->active_cuit, $business);
            $business = Business::query()->where(['client_access_token' => $request->header('AuthorizationBusiness')])->first();
        }
        $request->tactica_sync_valid = false;
        $user = $business->active_cuit->users[0];
        $request->cuit = $business->active_cuit->cuit;
        $request->email = $user->email;
        // Fuerza login al primer usuario de la empresa
        (new PassportController)->login($request, true)->content();

        return $next($request);
    }
}
