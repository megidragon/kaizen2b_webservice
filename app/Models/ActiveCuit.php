<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActiveCuit extends Model
{
    protected  $primaryKey = 'id';
    protected $table = 'active_cuit';

    public function users(){
        return $this->hasMany('App\Models\User', 'cuit_id', 'id');
    }

    public function business(){
        return $this->hasOne('App\Models\Business', 'cuit_id', 'id');
    }
}
