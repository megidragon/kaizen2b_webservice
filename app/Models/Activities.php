<?php

namespace App\Models;

use App\AbstractClass\ModuleModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;
use App\Models\Logs;

class Activities extends ModuleModel
{
    public $timestamps = false;
    protected $connection = 'custom';
    protected $table = 'activities';

    public function columns(){
        return $this->select([
            'id',
            DB::raw('name'),
            'use_alert',
            'datetime',
            'datetime_alert',
            'description',
            'business',
            'contact',
            'priority',
            'start',
            'end',
            'type',
            'phone',
            'location',
        ]);
    }

    public static function getSyncSql(): string
    {
        return str_replace('
        ', '', "
          SELECT
            actividades.RecID AS tactica_id,
            actividades.Referencia AS name,
            actividades.ConAlarma AS use_alert,
            actividades.Fecha AS datetime,
            actividades.FechaHoraAlarma AS datetime_alert,
            actividades.Notas AS description,
            actividades.Empresa AS business,
            actividades.Nombre AS contact,
            actividades.Prioridad AS priority,
            actividades.Inicio AS start,
            actividades.Fin AS end,
            actividades.Actividad AS type,
            '-' AS phone,
            actividades.Ubicacion AS location,
            actividades.ConHora AS use_hour,
            U.Usuario AS usuario
          FROM actividades
          LEFT JOIN usuarios U on actividades.IDUsuario = U.RecID
         WHERE 1 ");
    }

    public static function getTacticaTableName(): string
    {
        return "actividades";
    }

    public static function onInsert($row): bool
    {
        $moduleInstance = (new self)
            ->connect(auth()->user()->active_cuit->business->db_name)
            ->where(['tactica_id' => $row['data']['tactica_id']])
            ->first();
        // Si existe no tengo nada que hacer aca
        if (!empty($moduleInstance))
        {
            return false;
        }

        $moduleInstance = (new self)->connect(auth()->user()->active_cuit->business->db_name);

        if (!empty($row['data']['usuario'])){
            $user_id = User::query()->where(['name' => $row['data']['usuario'], 'cuit_id' => auth()->user()->cuit_id])->first();
            if (empty($user_id))
            {
                Logs::log("Username {$row['data']['usuario']} not found in server", 'warning', 'Insert activitie');
                $user_id = null;
            }else {
                $user_id = $user_id->id;
            }

        }else{
            $user_id = null;
        }

        $data = [
            "tactica_id"    => $row['data']['tactica_id'],
            "name"          => $row['data']['name'],
            "use_alert"     => $row['data']['use_alert'],
            "datetime"      => $row['data']['datetime'],
            "datetime_alert"=> $row['data']['datetime_alert'],
            "description"   => $row['data']['description'],
            "business"      => $row['data']['business'],
            "contact"       => $row['data']['contact'],
            "priority"      => $row['data']['priority'],
            "start"         => $row['data']['start'],
            "end"           => $row['data']['end'],
            "type"          => $row['data']['type'],
            "phone"         => $row['data']['phone'],
            "location"      => $row['data']['location'],
            "user_id"       => $user_id,
        ];

        return self::dynamicInsertUpdate($data, $moduleInstance);
    }

    public static function onUpdate($row): bool
    {
        $moduleInstance = (new self)
            ->connect(auth()->user()->active_cuit->business->db_name)
            ->where(['tactica_id' => $row['data']['tactica_id']])
            ->first();

        if (!empty($row['data']['usuario'])){
            $user_id = User::query()->where(['name' => $row['data']['usuario'], 'cuit_id' => auth()->user()->cuit_id])->first();
            if (empty($user_id))
            {
                throw new Exception("Username {$row['data']['usuario']} not found in server", 'warning', 'Insert activitie');
            }

            $user_id  = $user_id->id;
        }else{
            $user_id = null;
        }

        $data = [
            "tactica_id"    => $row['data']['tactica_id'],
            "name"          => $row['data']['name'],
            "use_alert"     => $row['data']['use_alert'],
            "datetime"      => $row['data']['datetime'],
            "datetime_alert"=> $row['data']['datetime_alert'],
            "description"   => $row['data']['description'],
            "business"      => $row['data']['business'],
            "contact"       => $row['data']['contact'],
            "priority"      => $row['data']['priority'],
            "start"         => $row['data']['start'],
            "end"           => $row['data']['end'],
            "type"          => $row['data']['type'],
            "phone"         => $row['data']['phone'],
            "location"      => $row['data']['location'],
            "user_id"       => $user_id,
        ];

        return self::dynamicInsertUpdate($data, $moduleInstance);
    }

    public static function onDelete($row): bool
    {
        $moduleInstance = (new self)
            ->connect(auth()->user()->active_cuit->business->db_name)
            ->where(['tactica_id' => $row['data']['tactica_id']])
            ->first();

        if (!empty($moduleInstance))
        {
            $moduleInstance->delete();
        }

        return true;
    }
}
