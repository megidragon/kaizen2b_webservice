<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Mockery\Exception;

class Auditory extends Model
{
    protected $connection = 'mysql';
    protected $table = 'auditory';

    public function user(){
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public static function logAction($action, $user_id)
    {
        try {
            $audit = new self;
            $audit->action = $action;
            $audit->user_id = $user_id;
            $audit->save();
            return true;
        }catch (Exception $e)
        {
            return false;
        }
    }
}
