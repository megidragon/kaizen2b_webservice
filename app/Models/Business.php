<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Mockery\Exception;

class Business extends Model
{
    protected $connection = 'mysql';
    protected  $primaryKey = 'id';
    protected $table = 'business';
    protected $casts = [
        'contacts' => 'array'
    ];

    public function active_cuit(){
        return $this->belongsTo('App\Models\ActiveCuit', 'cuit_id', 'id');
    }

    public function service_type()
    {
        return $this->hasOne('App\Models\ServiceType', 'key', 'service_key');
    }

    public function users(){
        return $this->hasMany('App\Models\User', 'cuit_id', 'cuit_id');
    }

    public function business_module(){
        return $this->hasMany('App\Models\BusinessModules', 'business_id', 'id')->with('module');
    }

    /**
     * @param array $modules
     */
    public function assignModules(array $modules)
    {
        if (empty($this)){
            throw new Exception("Busines was not defined");
        }

        foreach ($modules as $module)
        {
            $module_entity = Modules::where(['key' => $module])->first();
            if (!empty($module_entity) && !empty($this->id))
            {
                $businessModule = new BusinessModules();
                $businessModule->business_id = $this->id;
                $businessModule->module_id = $module_entity->id;
                $businessModule->save();
            }
            elseif(empty($this->id))
            {
                throw new Exception("Business not found");
            }
            else
            {
                throw new Exception("Module $module not found");
            }
        }

        return $this;
    }

    public function getModuleKeys($sync=false)
    {
        return array_filter(array_map(function($bm) use ($sync){
            if (!$sync || ($sync && $bm['module']['sync_enabled'])){
                return $bm['module']['key'];
            }
        }, $this->business_module->toArray()));
    }

    public function getDate($key) {
        return (new Carbon($this->$key))->format('d/m/y');
    }
}
