<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BusinessModules extends Model
{
    protected $table = 'business_modules';
    public $timestamps = false;
    public $primaryKey = 'business_id';

    public function module(){
        return $this->hasOne('App\Models\Modules', 'id', 'module_id');
    }

    public function business(){
        return $this->hasOne('App\Models\Business', 'id', 'business_id');
    }
}
