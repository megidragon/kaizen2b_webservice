<?php

namespace App\Models;

use App\AbstractClass\ModuleModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BusinessTactica extends ModuleModel
{
    public $timestamps = false;
    protected $connection = 'mysql';
    protected $table = 'business_tactica';
    protected $casts = [
        'contacts' => 'array'
    ];

    public static function getBusiness($id=null){
        // TODO: en la tabla empresas hay tanto empresas como proveedores, es encesario filtrar los proveedores
        $query = self::query();
        if ($id){
            return $query->where(['tactica_id' => $id])->first();
        }else{
            return $query->where('document', '!=', 'NULL')->get();
        }
    }

    public static function getSyncSql(): string
    {
        return str_replace('
        ', '', "
          SELECT
            empresas.RecID AS tactica_id,
            empresas.Empresa AS empresa,
            empresas.SitioWeb AS web_site,
            empresas.Calificacion AS qualification,
            empresas.Bloqueado AS blocked,
            empresas.Eliminado AS deleted,
            empresas.Fuente AS origin,
            F.NroImpuesto1 AS document,"
            .DB::raw("(SELECT GROUP_CONCAT(CONCAT(C.Nombre,' ', C.Apellido) SEPARATOR ', ') FROM contactos C WHERE C.IDEmpresa = empresas.IDEmpresa) as contacts,").
            "empresas.FechaCreacion AS created_at,
            empresas.FechaModificacion AS updated_at
          FROM empresas
          LEFT JOIN fiscal as F on F.IDref = empresas.IDEmpresa
          LEFT JOIN contactos as C on C.IDEmpresa = empresas.RecID
         WHERE 1 ");
    }

    public static function getTacticaTableName(): string
    {
        return 'empresas';
    }

    public static function onInsert($row): bool
    {
        $moduleInstance = (new self)
            ->connect(env('DB_DATABASE'))
            ->where(['tactica_id' => $row['data']['tactica_id']])
            ->first();
        // Si existe no tengo nada que hacer aca
        if (!empty($moduleInstance))
        {
            return false;
        }

        $moduleInstance = (new self)->connect(auth()->user()->active_cuit->business->db_name);

        if ($row['data']['contacts']){
            $contacts = explode(', ', $row['data']['contacts']);
        }else{
            $contacts = [];
        }

        $data = [
            "tactica_id"    => $row['data']['tactica_id'],
            "empresa"       => $row['data']['empresa'],
            "web_site"      => $row['data']['web_site'],
            "qualification" => $row['data']['qualification'],
            "blocked"       => $row['data']['blocked'],
            "deleted"       => $row['data']['deleted'],
            "origin"        => $row['data']['origin'],
            "document"      => $row['data']['document'],
            "contacts"      => $contacts,
            "created_at"    => $row['data']['created_at'],
            "updated_at"    => $row['data']['updated_at'],
        ];

        return self::dynamicInsertUpdate($data, $moduleInstance);
    }

    public static function onUpdate($row): bool
    {
        $moduleInstance = (new self)
            ->connect(auth()->user()->active_cuit->business->db_name)
            ->where(['tactica_id' => $row['data']['tactica_id']])
            ->first();

        if ($row['data']['contacts']){
            $contacts = explode(', ', $row['data']['contacts']);
        }else{
            $contacts = [];
        }

        $data = [
            "tactica_id"    => $row['data']['tactica_id'],
            "empresa"       => $row['data']['empresa'],
            "web_site"      => $row['data']['web_site'],
            "qualification" => $row['data']['qualification'],
            "blocked"       => $row['data']['blocked'],
            "deleted"       => $row['data']['deleted'],
            "origin"        => $row['data']['origin'],
            "document"      => $row['data']['document'],
            "contacts"      => $contacts,
            "created_at"    => $row['data']['created_at'],
            "updated_at"    => $row['data']['updated_at'],
        ];

        return self::dynamicInsertUpdate($data, $moduleInstance);
    }

    public static function onDelete($row): bool
    {
        $moduleInstance = (new self)
            ->connect(auth()->user()->active_cuit->business->db_name)
            ->where(['tactica_id' => $row['data']['tactica_id']])
            ->first();

        if (!empty($moduleInstance))
        {
            $moduleInstance->delete();
        }

        return true;
    }
}
