<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Mockery\Exception;

class ExportQeue extends Model
{
    protected $connection = 'mysql';
    protected $table = 'export_queue';
}
