<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Mockery\Exception;

class Logs extends Model
{
    protected $connection = 'mysql';
    protected $table = 'logs';

    public function user(){
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public static function log($msg, $type=null, $description=null, $tag=null, $time=null, $user_id=null)
    {
        $log = new self;
        $log->msg = $msg;
        $log->description = $description;
        $log->type = $type;
        $log->tag = $tag;
        $log->datetime = $time ? $time : date('Y-m-d H:i:s');
        $log->user_id = $user_id;
        $log->save();
        return true;
    }
}
