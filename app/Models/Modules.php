<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Modules extends Model
{
    protected $table = 'modules';
    public $timestamps = false;

    public static function getModules(){
        return self::query()->where('display', true)->get();
    }
}
