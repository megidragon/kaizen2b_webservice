<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class Params extends Model
{
    protected $connection = 'mysql';
    public $timestamps = false;
    protected $table = 'params';

    public static function getParams()
    {
        $result = self::query()->where(['current_version'=>true])->orderBy('id', 'desc')->first();
        return $result;
    }

    public static function getParamsHistory()
    {
        return self::query()->with('user')->orderBy('id', 'desc')->get();
    }

    public static function getGlobalAppVersion()
    {
        if (auth()->user()) {
            $busines = auth()->user()->active_cuit->business;
        }
        return self::query()->orderBy('id', 'desc')->first()->global_app_version;
    }

    public function getDate() {
        return (new Carbon($this->created_at))->format('d/m/y H:i:s');
    }

    public static function cleanOldRows(){
        DB::statement("UPDATE params SET current_version = 0;");
    }

    public static function versionExists($version){
        return self::query()->where(['global_app_version' => $version])->count() ? true : false;
    }

    public function user() {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
}
