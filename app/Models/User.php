<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Config;


class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $table = 'users';
    protected $connection = 'mysql';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'cuit_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'roles' => 'array',
    ];

    /***
     * @param string $role
     * @return $this
     */
    public function addRole(string $role)
    {
        $roles = $this->getRoles();
        $roles[] = $role;

        $roles = array_unique($roles);
        $this->setRoles($roles);

        return $this;
    }

    /**
     * @param array $roles
     * @return $this
     */
    public function setRoles(array $roles)
    {
        $this->setAttribute('roles', $roles);
        return $this;
    }

    /***
     * @param $role
     * @return mixed
     */
    public function hasRole($role)
    {
        return in_array($role, $this->getRoles());
    }

    /***
     * @param $roles
     * @return mixed
     */
    public function hasRoles($roles)
    {
        $currentRoles = $this->getRoles();
        foreach($roles as $role) {
            if ( ! in_array($role, $currentRoles )) {
                return false;
            }
        }
        return true;
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        $roles = $this->getAttribute('roles');

        if (is_null($roles)) {
            $roles = [];
        }

        return $roles;
    }

    public function active_cuit(){
        return $this->belongsTo('App\Models\ActiveCuit', 'cuit_id', 'id');
    }

    public function oauth_acess_token(){
        return $this->hasMany('App\Models\OauthAccessToken', 'user_id', 'id');
    }

    public function connect($dbname){
        Config::set("database.connections.custom", [
            "host" => env('DB_HOST'),
            "database" => $dbname,
            "username" => env('DB_USERNAME'),
            "password" => env('DB_PASSWORD'),
            'driver' => 'mysql',
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'prefix_indexes' => true,
            'strict' => true,
            'engine' => null
        ]);

        $this->connection = 'custom';

        return $this;
    }

    public static function getSyncSql(): string
    {
        $business_name = str_replace(' ', '-', auth()->user()->active_cuit->business->empresa);
        $cuit_id = auth()->user()->active_cuit->id;
        return str_replace('
        ', '', "
          SELECT
           usuarios.Usuario AS name,
           C.Correo AS email,
           NULL AS email_verified_at,
           NULL AS password,
           $cuit_id AS cuit_id,
           usuarios.RecID AS tactica_id
          FROM usuarios
          LEFT JOIN contactos C ON C.IDContacto = usuarios.IDContacto
          WHERE C.Correo IS NOT NULL 
          ");
    }

    public static function getTacticaTableName(): string
    {
        return "usuarios";
    }

    /**
     * @return array
     */
    public function getColumns(): array
    {
        return DB::getSchemaBuilder()->getColumnListing($this->table);
    }

    public function getDate($key) {
        return (new Carbon($this->$key))->format('d/m/y');
    }

    public static function makeSync($row)
    {
        // Verifica si tiene email
        if (empty($row['data']['email'])){
            return;
        }

        /*
         * Primero sincroniza en la db de la empresa Luego  una copia en la db principal
         */
        for ($i = 0; $i < 2; $i++) {
            // Primera vez ejecuta por la db de la empresa la segunda deja copia en la db del webservice para usar la gestion de sesiones
            if ($i >= 1) {
                if (auth()->user()->active_cuit->business->db_name === env('DB_DATABASE')) { // Esto prevee q se corra 2 veces si estoy sincronizando a la empresa principal
                    break;
                }
                $dbname = env('DB_DATABASE');
            }else{
                $dbname = auth()->user()->active_cuit->business->db_name;
            }

            if ($dbname === env('DB_DATABASE')) {
                $userModel = new static();
            }else{
                $userModel = (new static())->connect($dbname);
            }

            if ($row['operation_type'] == 'insert') {
                $result = $userModel->where(['email' => $row['data']['email']])->first();

                // Si existe no tengo nada que hacer aca
                if (!empty($result)) {
                    continue;
                }

            } elseif ($row['operation_type'] == 'update') {
                $userModel = $userModel->where(['tactica_id' => $row['data']['tactica_id']])->first();
            } elseif ($row['operation_type'] == 'delete') {
                $result = $userModel->where(['tactica_id' => $row['data']['tactica_id']])->first();
                if (empty($result)) {
                    continue;
                }
                $result->delete();
                continue;
            }

            $userModel->name = $row['data']['name'];
            $userModel->email = $row['data']['email'];
            $userModel->cuit_id = $row['data']['cuit_id'];
            $userModel->tactica_id = $row['data']['tactica_id'];
            $userModel->save();
        }
    }
}
