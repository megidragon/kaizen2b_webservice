<?php

namespace App\Role;

use App\Models\User;

/**
 * Class RoleChecker
 * @package App\Role
 */
class RoleChecker
{
    /**
     * @param User $user
     * @param string $role
     * @return bool
     */
    public function check($user, string $role)
    {
        if (empty($user)){
            return false;
        }

        // Admin has everything
        if ($user->hasRole(UserRole::ROLE_ADMIN) || $user->hasRole(UserRole::ROLE_SUB_ADMIN)) {
            return true;
        }

        return $user->hasRole($role);
    }
}