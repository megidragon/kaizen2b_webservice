<?php

namespace App\Role;

/***
 * Class UserRole
 * @package App\Role
 */
class UserRole {

    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_SUB_ADMIN = 'ROLE_SUB_ADMIN';
    const ROLE_CLIENT_PRO = 'ROLE_CLIENT_PRO';
    const ROLE_CLIENT = 'ROLE_CLIENT';

    /**
     * @var array
     */
    protected static $roleHierarchy = [
        self::ROLE_ADMIN => ['*'],
        self::ROLE_SUB_ADMIN => [
            self::ROLE_CLIENT_PRO
        ],
        self::ROLE_CLIENT_PRO => [
            self::ROLE_CLIENT
        ],
        self::ROLE_CLIENT => [
            'activities'
        ]
    ];

    /**
     * @param string $role
     * @return array
     */
    public static function getAllowedRoles(string $role)
    {
        if (isset(self::$roleHierarchy[$role])) {
            return self::$roleHierarchy[$role];
        }

        return [];
    }

    /***
     * @return array
     */
    public static function getRoleList()
    {
        return [
            static::ROLE_ADMIN =>'Admin',
            static::ROLE_SUB_ADMIN => 'Management',
            static::ROLE_CLIENT_PRO => 'Client Pro',
            static::ROLE_CLIENT => 'Client',
        ];
    }

}
