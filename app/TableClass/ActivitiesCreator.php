<?php

namespace App\TableClass;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Config;

class ActivitiesCreator
{
    /**
     * @param $dbname
     */
    public static function createTable($dbname)
    {
        Config::set("database.connections.custom", [
            "host" => env('DB_HOST'),
            "database" => $dbname,
            "username" => env('DB_USERNAME'),
            "password" => env('DB_PASSWORD'),
            'driver' => 'mysql',
        ]);

        Schema::connection('custom')->create('activities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("tactica_id")->nullable(); // "Alerta 1"
            $table->string("name")->nullable(); // "Alerta 1"
            $table->boolean("use_alert")->default(0); // true
            $table->dateTime( "datetime")->nullable(); // "2019-03-11 16:00:00"
            $table->dateTime("datetime_alert")->nullable(); // "2019-03-11 16:00:00"
            $table->text("description")->nullable(); // "Descripcion de prueba"
            $table->string("business")->nullable(); // "Empresa A"
            $table->string("contact")->nullable(); // "John Doe"
            $table->integer("priority")->default(1); // "1"
            $table->integer("start")->nullable(); // 600
            $table->integer("end")->nullable(); // 650
            $table->string("type")->nullable(); // "Alerta"
            $table->string("phone")->nullable(); // "11 1212 1444"
            $table->string("location")->nullable(); // "Oficina Cliente"
            $table->string("user_id")->nullable();
            $table->boolean("use_hour")->default(0);
        });
    }
}
