<?php

namespace App\TableClass;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Config;

class UsersCreator
{
    /**
     * @param $dbname
     */
    public static function createTable($dbname)
    {
        Config::set("database.connections.custom", [
            "host" => env('DB_HOST'),
            "database" => $dbname,
            "username" => env('DB_USERNAME'),
            "password" => env('DB_PASSWORD'),
            'driver' => 'mysql',
        ]);

        Schema::connection('custom')->create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->boolean('status')->default(1);
            $table->text('roles')->nullable();
            $table->string('last_hash')->nullable();
            $table->string('tactica_id')->nullable();
            $table->bigInteger('cuit_id');
            $table->rememberToken();
            $table->timestamps();
        });
    }
}
