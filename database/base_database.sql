-- MySQL dump 10.13  Distrib 5.7.26, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: kaizen2b_webservice
-- ------------------------------------------------------
-- Server version	5.7.26-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `active_cuit`
--

DROP TABLE IF EXISTS `active_cuit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `active_cuit` (
  `tactica_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cuit` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expire_date` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  UNIQUE KEY `active_cuit_cuit_unique` (`cuit`),
  KEY `active_cuit_cuit_index` (`cuit`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `active_cuit`
--

LOCK TABLES `active_cuit` WRITE;
/*!40000 ALTER TABLE `active_cuit` DISABLE KEYS */;
INSERT INTO `active_cuit` (`tactica_id`, `cuit`, `name`, `expire_date`, `created_at`, `updated_at`) VALUES ('','11111111111','Empresa A','2020-01-01 00:00:00','2019-04-10 17:39:15','2019-04-10 17:39:15');
/*!40000 ALTER TABLE `active_cuit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `activities`
--

DROP TABLE IF EXISTS `activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activities` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tactica_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `use_alert` tinyint(1) NOT NULL,
  `datetime` datetime NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `business` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `priority` int(11) NOT NULL,
  `start` int(11) NOT NULL,
  `end` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activities`
--

LOCK TABLES `activities` WRITE;
/*!40000 ALTER TABLE `activities` DISABLE KEYS */;
/*!40000 ALTER TABLE `activities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_access_tokens`
--

LOCK TABLES `oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES ('001eb56b124388a8b2bcdb59cc479f659144e74201fa1eb69ca1eb6fa948bbf7243cc36a5f697127',2,1,'custom_token','[]',0,'2019-04-16 22:57:19','2019-04-16 22:57:19','2020-04-16 19:57:19'),('05cc10acabb00d2d26bc75d59fa9472c23c29ef7c325d2034aa0db55e448b152f8df155330ae9f47',2,1,'custom_token','[]',0,'2019-05-01 16:42:40','2019-05-01 16:42:40','2020-05-01 13:42:40'),('068da08135236d262c544d66a4de864cbfccba0a79456b2049411c971cc0636f26bfbbd8fca6fc80',2,1,'custom_token','[]',0,'2019-05-01 16:56:27','2019-05-01 16:56:27','2020-05-01 13:56:27'),('1df5ae1d0935cc8ad9215f18c996d04c91de3d4f87bfa45ce7b2e2e8c21b033dc261fb73504f27d8',2,1,'custom_token','[]',0,'2019-05-13 18:01:21','2019-05-13 18:01:21','2020-05-13 15:01:21'),('279454f543b20c89291e83cd5e74f74b1d5c5644245dc8f245ed5aff7611a3a3cb61ed90ddff6209',2,1,'custom_token','[]',0,'2019-05-01 17:10:31','2019-05-01 17:10:31','2020-05-01 14:10:31'),('319b7dbfa8d156d4df945b32905b33f8795eea3a6b547f76a734e20e34ce93d4441855f3677a035b',2,1,'custom_token','[]',0,'2019-05-01 15:18:06','2019-05-01 15:18:06','2020-05-01 12:18:06'),('39c393d71899df186747ea456df9451d4691501baadf9a72b678f444f9e6abf9bc0d4cea2e47a9d6',2,1,'custom_token','[]',0,'2019-04-29 22:36:11','2019-04-29 22:36:11','2020-04-29 19:36:11'),('3d3a31a09988aa8a04cc713cb226e00de1f5a1dc11e8270e386854b4f7e72ed14ddcf712eb5edf4f',2,1,'custom_token','[]',0,'2019-04-29 22:38:58','2019-04-29 22:38:58','2020-04-29 19:38:58'),('3e18220eaf3cdaab66b548faaaeb75fddfe47e557e35e04ca1c36c5b7b682cd6f1797e3c728ffddf',2,1,'custom_token','[]',0,'2019-05-01 17:11:39','2019-05-01 17:11:39','2020-05-01 14:11:39'),('405978c4b3d8415df833e08b2c5b4bae904d0a5dec4a6a00e1a71d5d47d52312469ca06d92d07d5e',2,1,'custom_token','[]',0,'2019-05-02 17:12:00','2019-05-02 17:12:00','2020-05-02 14:12:00'),('4116530f99f3d79e7086da0fefc4c5e87d64ff9d7fa538ce2e133d4eda0019cacaf13144455f9786',2,1,'custom_token','[]',0,'2019-05-01 17:11:39','2019-05-01 17:11:39','2020-05-01 14:11:39'),('4142e3240e5cf697cd7c9fe69feab6ebccc6e044d22d88e241c3355aed1fcfb3e41c288577d1ee1b',2,1,'custom_token','[]',0,'2019-05-01 17:15:53','2019-05-01 17:15:53','2020-05-01 14:15:53'),('4fee7457b171356a07d20c567a8d9d35985350dc6ff8b5bd9729b8d5821b90e35a44d45ca3f955eb',2,1,'custom_token','[]',0,'2019-04-29 22:36:38','2019-04-29 22:36:38','2020-04-29 19:36:38'),('508f80a3f388d5ea58e32ec934b065772efc7bcfb34ce70214f487082ea5cfe40310c3fed3782806',2,1,'custom_token','[]',0,'2019-05-01 17:10:26','2019-05-01 17:10:26','2020-05-01 14:10:26'),('52268212da0a1be4b2f450243c974d9d42991dd88a436bdfd23ae1691feddd2b232d0f953f9e44cc',2,1,'custom_token','[]',0,'2019-04-29 22:38:42','2019-04-29 22:38:42','2020-04-29 19:38:42'),('658b4d70b5528176d5adb36677c1f2020d75b2bdfd7265cc1f2125861411bc1375b5d2a5e0f7c609',2,1,'custom_token','[]',0,'2019-05-01 15:13:22','2019-05-01 15:13:22','2020-05-01 12:13:22'),('76d38d6cf6c79abf6392b43dd12c3d6158155202340427a20fc5d364a98556000aac8a06c76ce891',4,1,'custom_token','[]',0,'2019-05-13 17:44:21','2019-05-13 17:44:21','2020-05-13 14:44:21'),('7c8360dca371ef177b8821977703f289115f561357271d7d450865faf9fe1d75cb93618b4efdcae6',2,1,'custom_token','[]',0,'2019-04-16 23:23:20','2019-04-16 23:23:20','2020-04-16 20:23:20'),('7dc31a3080854017639dd4ec2ab0f7ac7381d0c302128d90e8802d1f0dfdc8216add62830ecba6e8',2,1,'custom_token','[]',0,'2019-05-02 15:57:26','2019-05-02 15:57:26','2020-05-02 12:57:26'),('851dd4d86d82bfbace7cab931f1b6a2527905a95aa563bfcd6329e51f9d6dea139a1ec406359236f',2,1,'custom_token','[]',0,'2019-05-03 23:43:44','2019-05-03 23:43:44','2020-05-03 20:43:44'),('87ddfc77f9191f43c779b108c7e2c89fe226f48ead65b13b32cf18a4afd3c3eaf288457e0ab71065',2,1,'custom_token','[]',0,'2019-04-17 19:51:13','2019-04-17 19:51:13','2020-04-17 16:51:13'),('89af0b75c5da7730fd5aa93ccad7f3811b03714eb19f0db7a1d4a60fc46c3877d0a7292f0b7f2873',2,1,'custom_token','[]',0,'2019-05-13 17:51:49','2019-05-13 17:51:49','2020-05-13 14:51:49'),('92114e07b70feb4fb02422385eeeb9a3a02362574deb5d9f0112c75b9ef648007234981aa3fdc32d',2,1,'custom_token','[]',0,'2019-05-01 17:07:58','2019-05-01 17:07:58','2020-05-01 14:07:58'),('a028e5baacd6868facba873ebf088cd1de9cb3308563139bd3a29458918f7078acad82d997c1f59a',2,1,'custom_token','[]',0,'2019-05-01 17:11:57','2019-05-01 17:11:57','2020-05-01 14:11:57'),('a3a8ddd839bb17c560b61a1f2156f95f1dbf5e646c838d3305b873f8f2cca92907009d7e87b42e5e',2,1,'custom_token','[]',0,'2019-04-10 17:41:53','2019-04-10 17:41:53','2020-04-10 14:41:53'),('a9450b04a17de261d44fece064b8af830e16b298e36ee055a27308b67a3aff92c4aeffcdfe4dd605',2,1,'custom_token','[]',0,'2019-05-01 16:41:28','2019-05-01 16:41:28','2020-05-01 13:41:28'),('ab1563f40183ba7392eb0247c41d8a35b90a9100eed3c6cb329d9291e9dc62e4eff50ed4471e0f5c',2,1,'custom_token','[]',0,'2019-05-01 17:01:54','2019-05-01 17:01:54','2020-05-01 14:01:54'),('ae20b4599c3aadd22efb03588833b719d6fb1625babb5aa02060012c8e4c1357a6337998c7924540',2,1,'custom_token','[]',0,'2019-05-01 17:12:47','2019-05-01 17:12:47','2020-05-01 14:12:47'),('afac6a3beb67d0299f4b39b6c12f2a07f879baa83e6c0a1a42c29539999bb750b9b2305d9ae4eacd',2,1,'abc789','[]',0,'2019-05-01 17:36:28','2019-05-01 17:36:28','2020-05-01 14:36:28'),('b07c52126cd1728a27bc759d54f75142f7824f2d457d75ef25396483010197389f316b2a112378ef',2,1,'custom_token','[]',0,'2019-05-01 17:37:33','2019-05-01 17:37:33','2020-05-01 14:37:33'),('ce6f6f9f2c8671f8a46c201dbe9a5fab8bc28bd643708396cc91bc7ad539d0a422f29c2cfc5530ef',2,1,'custom_token','[]',0,'2019-05-01 17:11:31','2019-05-01 17:11:31','2020-05-01 14:11:31'),('cf62cb1c4cb735c158eba635c40090bd2c1d2a218373df1ef9a351cd9a9b73e2cc3b5d990e0bed36',2,1,'custom_token','[]',0,'2019-05-01 15:13:59','2019-05-01 15:13:59','2020-05-01 12:13:59'),('d1a4bf5276dc775e80a636297f4bcb7517ad2d64de6c57877d642328fe07db8b32a1a5e015c4b4d5',2,1,'custom_token','[]',0,'2019-05-01 15:17:46','2019-05-01 15:17:46','2020-05-01 12:17:46'),('d5cffc2786188f27ca8e5c8f83d53e0ffd11eabdcb06e47eb78fffd17a26e690a84d8b9d81e794c2',2,1,'custom_token','[]',0,'2019-05-01 17:11:31','2019-05-01 17:11:31','2020-05-01 14:11:31'),('dbbe3ae0083562fd8bd98335260d3ac6db5c5a671c7e0977f6a674298f5a4cf6b22da788868d8ad3',2,1,'custom_token','[]',0,'2019-04-29 22:37:42','2019-04-29 22:37:42','2020-04-29 19:37:42'),('ddbf214b3ba813c1286daba82a060fabaa9e96e394d1d48bb34b4cf44ee18b9c459da454eab4f17c',2,1,'custom_token','[]',0,'2019-05-13 17:55:43','2019-05-13 17:55:43','2020-05-13 14:55:43'),('ef978c29e128ced52ba000f561937baf2c0624abcf88ccd55467e1d7c608f01217ed0866581911c9',2,1,'custom_token','[]',0,'2019-04-29 22:34:58','2019-04-29 22:34:58','2020-04-29 19:34:58'),('f575e7a6fe67f2252cfef5f02835cbea264772b5b250887207b774cdeaaba9c691977c13aac5a039',2,1,'custom_token','[]',0,'2019-05-01 15:14:21','2019-05-01 15:14:21','2020-05-01 12:14:21'),('f7922cf76ac020b5b6ca9e157a972c97508f94c3fcc19def3284c16264394f6f9bfaeb67c76345e0',2,1,'custom_token','[]',0,'2019-05-01 17:06:29','2019-05-01 17:06:29','2020-05-01 14:06:29'),('f93756aaddc349d41d7bbacf7438b5f187d0090bc92e00265d1048f1b17eb99ab5caf227aa139ca5',4,1,'custom_token','[]',0,'2019-05-13 17:55:52','2019-05-13 17:55:52','2020-05-13 14:55:52'),('fd7af458cfccd743e009ebff6e41a96910147b4b1b2c64716aa5a0c4f60835ef9b21c34be4962fe0',2,1,'custom_token','[]',0,'2019-05-01 16:56:49','2019-05-01 16:56:49','2020-05-01 13:56:49'),('fe028c5ebba547b028e1982df9a7bc93130106cd7531c64f952e1f07a9717abd66d81870de786850',2,1,'custom_token','[]',0,'2019-05-01 17:04:18','2019-05-01 17:04:18','2020-05-01 14:04:18');
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(10) unsigned NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_auth_codes`
--

LOCK TABLES `oauth_auth_codes` WRITE;
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_clients`
--

LOCK TABLES `oauth_clients` WRITE;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES (1,NULL,'Laravel Personal Access Client','vrakqeS7Jh2XNQxhzfq5veoTJ4vvfvLXq1QVnWa6','http://localhost',1,0,0,'2019-04-10 17:41:33','2019-04-10 17:41:33'),(2,NULL,'Laravel Password Grant Client','NlDllJvyX0Dj5DKACSKXzB3XhH39c153zFrqiAwK','http://localhost',0,1,0,'2019-04-10 17:41:34','2019-04-10 17:41:34');
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_personal_access_clients`
--

LOCK TABLES `oauth_personal_access_clients` WRITE;
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES (1,1,'2019-04-10 17:41:33','2019-04-10 17:41:33');
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_refresh_tokens`
--

LOCK TABLES `oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `roles` text COLLATE utf8mb4_unicode_ci,
  `cuit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_cuit_foreign` (`cuit`),
  CONSTRAINT `users_cuit_foreign` FOREIGN KEY (`cuit`) REFERENCES `active_cuit` (`cuit`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `roles`, cuit) VALUES (1,'admin','admin@admin.com',NULL,'$2y$10$HOpialiJ3yXa/HBKoTADfODOWo.eUCFSt2vPQP3l/5UPasJBY2HwS',NULL,now(),now(),'[\"ROLE_CLIENT\"]','11111111111');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-13 14:40:07
