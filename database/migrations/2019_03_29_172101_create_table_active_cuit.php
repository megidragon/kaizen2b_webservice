<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableActiveCuit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('active_cuit', function (Blueprint $table) {
            $table->string('tactica_id');
            $table->string('cuit')->unique();
            $table->string('name');
            $table->dateTime('expire_date');
            $table->timestamps();
            $table->index('cuit');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->string('cuit')->nullable();

            $table->foreign('cuit')->references('cuit')->on('active_cuit')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table) {
            $table->dropForeign('users_cuit_foreign');
            $table->dropColumn('cuit');
        });
        Schema::dropIfExists('active_cuit');
    }
}
