<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_modules', function (Blueprint $table) {
            $table->bigInteger('business_id');
            $table->bigInteger('module_id');


        });

        Schema::create('business', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_tactica')->nullable();
            $table->string('client_access_token');
            $table->string('db_name');
            $table->string('sitio_web')->nullable();
            $table->boolean('service_status')->default(0);
            $table->string('cuit')->nullable();
            $table->string('service_key');
            $table->string('empresa')->nullable();
            $table->string('calificacion')->nullable();
            $table->string('contacto')->nullable();
            $table->boolean('sync_installed')->default(0);
            $table->boolean('use_custom_app_version')->default(0);
            $table->string('app_version')->nullable();
            $table->string('sync_client_ip')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_modules');
        Schema::dropIfExists('business');
    }
}
