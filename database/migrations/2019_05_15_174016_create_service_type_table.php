<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_type', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('key');
            $table->string('details');
        });

        DB::table('service_type')->insert([
                'name' => 'Gratiuta',
                'key' => 'free',
                'details' => 'Servicio gratuito'
            ]
        );
        DB::table('service_type')->insert([
                'name' => 'Pro',
                'key' => 'pro',
                'details' => 'Servicio completo'
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_type');
    }
}
