<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCuit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            DB::beginTransaction();
            Schema::table('users', function (Blueprint $table) {
                $table->dropForeign('users_cuit_foreign');
            });

            Schema::table('active_cuit', function (Blueprint $table) {
                $table->increments('id');
                $table->dropColumn('cuit');
            });

            Schema::table('active_cuit', function (Blueprint $table) {
                $table->string('cuit')->nullable();
            });

            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('cuit');

                $table->bigInteger('cuit_id');
                //$table->foreign('cuit_id')->references('id')->on('active_cuit')->onDelete('cascade');
            });

            Schema::table('business', function (Blueprint $table) {
                $table->dropColumn('cuit');
                $table->bigInteger('cuit_id');
            });

            DB::commit();
        }catch (Exception $e){
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_cuit_id_foreign');
            $table->dropColumn('cuit_id');

            $table->string('cuit')->nullable();
            $table->foreign('cuit')->references('id')->on('active_cuit')->onDelete('cascade');
        });
    }
}
