<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCurrentVersionConfig extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('params', function(Blueprint $table){
            $table->boolean('current_version')->default(0);
        });

        DB::statement("UPDATE params SET current_version=1 ORDER BY id DESC LIMIT 1;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('params', function(Blueprint $table){
            $table->dropColumn('current_version');
        });
    }
}
