<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessTactica extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_tactica', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tactica_id')->nullable();
            $table->string('empresa');
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('web_site')->nullable();
            $table->string('qualification')->nullable();
            $table->boolean('blocked')->default(0);
            $table->boolean('deleted')->default(0);
            $table->string('origin')->nullable();
            $table->string('document')->nullable();
            $table->longText('contacts')->nullable();
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
        });

        Schema::table('users', function(Blueprint $table){
            $table->string('email')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_tactica');
        Schema::table('users', function(Blueprint $table){
            $table->string('email')->change();
        });
    }
}
