<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserIdParams extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('params', function(Blueprint $table){
            $table->bigInteger('user_id')->nullable();
        });
        DB::statement("UPDATE params SET params.user_id = 1;");
        Schema::table('params', function(Blueprint $table){
            $table->bigInteger('user_id')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('params', function(Blueprint $table){
            $table->dropColumn('user_id');
        });
    }
}
