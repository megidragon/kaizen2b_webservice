<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSyncEnabledModules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('modules', function (Blueprint $table) {
            $table->boolean('sync_enabled')->default(true);
        });

        DB::table('modules')->insert([
                'name' => 'Mercadolibre',
                'key' => 'ml',
                'status' => true,
                'display' => true,
                'sync_enabled' => false,
            ]
        );

        DB::table('business_modules')->insert([
                'business_id' => 1,
                'module_id' => DB::table('modules')->where('key', 'ml')->first()->id,
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('modules', function (Blueprint $table) {
            $table->dropColumn('sync_enabled');
        });

        DB::table('modules')->where('key', 'ml')->delete();
    }
}
