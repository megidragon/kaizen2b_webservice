<?php

use App\Models\Business;
use App\Models\User;
use App\Role\UserRole;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{

    public function run()
    {

        //if (env('APP_ENV') != 'production') {
        $active_cuit = new \App\Models\ActiveCuit();
        $active_cuit->tactica_id = '';
        $active_cuit->cuit = '30712330984';
        $active_cuit->name = 'Kaizen2b Empresa';
        $active_cuit->expire_date = '2120-01-01';
        $active_cuit->save();

        $new_business = new Business();
        $new_business->id_tactica = '-';
        $new_business->client_access_token = \App\Http\Controllers\Admin\BusinessController::generateRandomString(42);
        $new_business->db_name = env('DB_DATABASE');
        $new_business->service_status = true;
        $new_business->cuit_id = 1;
        $new_business->service_key = 'pro';
        $new_business->sitio_web = '-';
        $new_business->contacts = ['Christian', 'Alberto'];
        $new_business->calificacion = '-';
        $new_business->empresa = 'Kaizen2b Empresa';
        $new_business->sync_installed = false;
        $new_business->sync_client_ip = null;

        // TODO: Falta añadir esta funcionalidad
        $new_business->use_custom_app_version = false;
        $new_business->app_version = null;

        $new_business->save();

        $newModelBusiness = new \App\Models\BusinessModules();
        $newModelBusiness->business_id = 1;
        $newModelBusiness->module_id = 1;
        $newModelBusiness->save();

        $newModelBusiness = new \App\Models\BusinessModules();
        $newModelBusiness->business_id = 1;
        $newModelBusiness->module_id = 2;
        $newModelBusiness->save();

        $password = Hash::make('AgusPablo2019');
        $user = new User;
        $user->name = 'admin';
        $user->email = 'admin@admin.com';
        $user->password = $password;
        $user->cuit_id = $active_cuit->id;
        $user->imei = null;
        $user->addRole(UserRole::ROLE_ADMIN);
        $user->save();
        //}
    }
}
