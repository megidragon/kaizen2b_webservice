page = 1;
lastResults = [];

$(document).ready(function(){
    $('body').on('click', '.openModal', function (e) {
        let i = $(this).data('index');
        let data = '<h3>Detalles</h3>' +
            '<table>' +
            '<tr><td>Fecha: </td><td>'+lastResults[i].datetime+'</td></tr>' +
            '<tr><td>Inicio: </td><td>'+lastResults[i].start+'</td></tr>' +
            '<tr><td>Fin: </td><td>'+lastResults[i].end+'</td></tr>' +
            '<tr><td>Tipo: </td><td>'+lastResults[i].type+'</td></tr>' +
            '<tr><td>Contacto: </td><td>'+lastResults[i].contact+'</td></tr>' +
            '<tr><td>Empresa: </td><td>'+lastResults[i].business+'</td></tr>' +
            '<tr><td>Tel: </td><td>'+lastResults[i].phone+'</td></tr>' +
            '<tr><td>Lugar: </td><td>'+lastResults[i].location+'</td></tr>' +
            '<tr><td colspan="2">Descripción: </td></tr><tr><td colspan="2">'+lastResults[i].description+'</td></tr>' +
            '</table>';
        $('#detailsModal .modal-title').text(lastResults[i].name);
        $('#detailsModal .modal-body').html(data);
    })
} );

request('GET', 'activities', {page: page, results: 60, fromDate: '2000-01-01'}, function(res){
    if (res.success)
    {
        $('#activities-datatable tbody').html('');
        if (res.results_count > 0)
        {
            lastResults = res.results;
            $.each(res.results, function(i, element)
            {
                $('#activities-datatable tbody').append('<tr>' +
                    '<td>'+element.datetime+'</td>' +
                    '<td>'+element.name+'</td>' +
                    '<td>'+element.type+'</td>' +
                    '<td>'+element.location+'</td>' +
                    '<td>' +
                    '<button ' +
                    '   class="btn btn-primary openModal" ' +
                    '   data-toggle="modal" ' +
                    '   data-target="#detailsModal"' +
                    '   data-index="'+i+'"' +
                    '><i class="fa fa-table"></i></button></td>' +
                    '</tr>')
            });
        }
        else
        {
            $('#activities-datatable tbody').html('<tr>\n' +
                '                                    <td>Sin actividades</td>\n' +
                '                                    <td></td>\n' +
                '                                    <td></td>\n' +
                '                                    <td></td>\n' +
                '                                    <td></td>\n' +
                '                                </tr>');
        }

        $('#activities-datatable').DataTable({
            pageLength: 10,
            paging: true,
            searching: true,
            order: [[0, "asc"]],
        });

    }else{
        alert(res.error.msg);
    }
});
