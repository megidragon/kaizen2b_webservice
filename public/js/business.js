$(document).ready(function(){
    orderBy = typeof orderBy !== 'undefined' ? orderBy : [[0, "desc"]];

    $('body').on('click', '.show-cat', function (e) {
        Swal.fire({
            type: 'info',
            title: 'Clave de acceso de sincronizacion',
            html: '<strong>Access token</strong>: <input type="text" class="form-control" readonly value="'+$(this).data('cat')+'">',
            showConfirmButton: true,
        });
    });

    $('body').on('click', '.business-down', function (e) {
        Swal.fire({
            title: '¿Esta seguro de realizar la accion??',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirmar'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: '/kzn-admin/active-business/delete/'+$(this).data('id'),
                    type: 'DELETE',
                    success: function(result) {
                        if (result.success) {
                            Swal.fire({
                                type: 'success',
                                title: 'Empresa dada de baja correctamente.',
                                html: 'En 3 segundos sera redireccionado...',
                                showConfirmButton: false,
                                timer: 3500
                            });
                            setTimeout(function () {
                                document.location.reload();
                            }, 3000);
                            return;
                        }
                        alert('Error inesperado');
                    },
                    error: function (err) {
                        console.log(err.responseJSON);
                        Swal.fire({
                            type: 'error',
                            title: 'Error al intentar dar de baja la empresa',
                            showConfirmButton: true,
                        });
                    }
                });
            }
        });
    });

    $('body').on('click', '.disable-user', function (e) {
        Swal.fire({
            title: '¿Esta seguro de realizar la accion??',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirmar'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: '/kzn-admin/active-business/users/disable/'+$(this).data('id'),
                    type: 'DELETE',
                    success: function(result) {
                        if (result.success) {
                            Swal.fire({
                                type: 'success',
                                title: 'Usuario dado de baja correctamente.',
                                html: 'En 3 segundos sera redireccionado...',
                                showConfirmButton: false,
                                timer: 3500
                            });
                            setTimeout(function () {
                                document.location.reload();
                            }, 3000);
                            return;
                        }
                        alert('Error inesperado');
                    },
                    error: function (err) {
                        console.log(err.responseJSON);
                        Swal.fire({
                            type: 'error',
                            title: 'Error al intentar dar de baja el usuario',
                            showConfirmButton: true,
                        });
                    }
                });
            }
        });
    });

    $('#datatable').DataTable({
        pageLength: 10,
        paging: true,
        searching: true,
        order: orderBy,
    });
});
