function request(method, url, data={}, callback){
    headers = {
        'Accept': 'application/json',
        "content-type": "application/x-www-form-urlencoded",
        'Authorization': readToken()
    };

    if (method.toLocaleLowerCase() == 'get'){
        $.ajax({
            type: 'GET',
            url: BASE_URL_WS+url,
            headers: headers,
            data: data,
            dataType: 'json',
            success: callback,
            fail: function (xhr, textStatus, errorThrown) {
                alert('request failed');
            }
        });
    }else {
        $.ajax({
            type: method,
            url: BASE_URL_WS + url,
            headers: headers,
            data: data,
            dataType: 'json',
            success: callback,
            fail: function (xhr, textStatus, errorThrown) {
                alert('request failed');
            }
        });
    }
}

function loginWs(cuit, email, pass, recaptcha, remember=false){
    grecaptcha.execute(reCAPTCHA_site_key, { action: 'login' }).then(function (token) {
        $('[name="g-recaptcha-response"]').val(token);
        $.post(BASE_URL+'login', {
            'cuit': cuit,
            'email': email,
            'password': pass,
            'g-recaptcha-response': recaptcha,
        },function (res) {
            if (res.success){
                if (res.new_user){
                    new_user_url = res.new_user_setup;
                    $('#NewUserModal').modal();
                    return;
                }
                Swal.fire({
                    position: 'top-end',
                    type: 'success',
                    title: 'En 3 segundos sera redireccionado.',
                    showConfirmButton: false,
                    timer: 3500
                });
                setTimeout(function () {
                    if (res.user_role == 'ROLE_ADMIN') {
                        document.location.replace('kzn-admin');
                    }else{
                        document.location.replace('activities');
                    }
                }, 3000)
            }else if(typeof res.error != 'undefined' && typeof res.error.msg != 'undefined'){
                alert('Error al iniciar sesion: '+res.error.msg);
            }else{
                alert('Error no identificado');
            }
        }, 'json').error(function (error) {
            Swal.fire({
                position: 'top-end',
                type: 'error',
                title: error.responseJSON.error.msg,
                showConfirmButton: false,
                timer: 2500
            });
        });
    }, function (reason) {
        console.log(reason);
    });
}

function logout(){
    eraseCookie('token');
}
