$(document).ready(function(){
    orderBy = typeof orderBy !== 'undefined' ? orderBy : [[0, "desc"]];

    $('body').on('click', '.delete-user', function (e) {
        let id = $(this).data('id');
        let url = '/kzn-admin/users/'+id;
        Swal.fire({
            title: '¿Esta seguro de eliminar este usuario?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirmar'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: url,
                    type: 'DELETE',
                    success: function(result) {
                        if (result.success) {
                            Swal.fire({
                                type: 'success',
                                title: 'Usuario eliminado',
                                html: 'En 3 segundos sera redireccionado...',
                                showConfirmButton: false,
                                timer: 3500
                            });
                            setTimeout(function () {
                                document.location.reload();
                            }, 3000);
                            return;
                        }else if (result.msg){

                        }
                        alert('Error inesperado');
                    },
                    error: function (err) {
                        Swal.fire({
                            type: 'error',
                            title: err.responseJSON.message,
                            showConfirmButton: true,
                        });
                    }
                });
            }
        });
    });

    $('#datatable').DataTable({
        pageLength: 10,
        paging: true,
        searching: true,
        order: orderBy,
    });
});
