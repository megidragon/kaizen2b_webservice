import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            name: 'activities',
            component: require('./views/Activities').default
        },
        {
            path: '/activities',
            name: 'activities',
            component: require('./views/Activities').default
        },
        {
            path: '/profile',
            name: 'profile',
            component: require('./views/Profile').default
        },
        {
            path: '*',
            component: require('./views/404').default
        },
    ],
    mode: 'history'
});