@extends('admin.layouts.app')

@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">

            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Listado de auditoria</h1>
                </div>
            </div>

            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Auditoria
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="datatable">
                                    <thead>
                                    <tr>
                                        <th width="15%">Fecha</th>
                                        <th>Accion</th>
                                        <th width="15%">Protagonista</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if (!empty($auditories))
                                        @foreach($auditories as $i => $audit)
                                            <tr>
                                                <td>{{$audit->created_at}}</td>
                                                <td>{{$audit->action}}</td>
                                                <td>{{$audit->user->name}}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td>Sin registros</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

            <!-- ... Your content goes here ... -->

        </div>
    </div>
    <script>
        lastResults = {!! json_encode($auditories) !!};
    </script>
    <script src="{{ asset('js/business.js') }}"></script>
@endsection
