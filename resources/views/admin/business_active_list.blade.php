@extends('admin.layouts.app')

@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">

            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Listado de empresas dadas de alta</h1>
                </div>
            </div>

            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Empresas ingresadas
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="datatable">
                                    <thead>
                                    <tr>
                                        <th width="10%">Fecha alta</th>
                                        <th>Cuit</th>
                                        <th>Empresa</th>
                                        <th>Contactos</th>
                                        <th>Estado del servicio</th>
                                        <th>Estado sincronizacion</th>
                                        <th>Tipo de servicio</th>
                                        <th width="10%">Ultima actualizacion</th>
                                        <th>Acciones</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if (!empty($empresas))
                                        @foreach($empresas as $i => $empresa)
                                            <tr>
                                                <td>{{$empresa->created_at}}</td>
                                                <td>{{$empresa->active_cuit->cuit}}</td>
                                                <td>{{$empresa->empresa}}</td>
                                                <td>{{$empresa->contacts && count((array)$empresa->contacts) > 2 ? $empresa->contacts[0].', '.$empresa->contacts[1].', ...' : implode(', ', (array)$empresa->contacts)}}</td>
                                                <td>{{$empresa->service_status ? 'Activo' : 'Inactivo'}}</td>
                                                <td>{{$empresa->sync_installed ? 'Sincronizado' : 'No sincronizo'}}</td>
                                                <td>{{$empresa->service_type->name}}</td>
                                                <td>{{$empresa->updated_at}}</td>
                                                <td>
                                                    <div class="dropdown">
                                                        <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Acciones
                                                            <span class="caret"></span></button>
                                                        <ul class="dropdown-menu">
                                                            <li>
                                                                <a>
                                                                    <button
                                                                            class="btn btn-success openModal"
                                                                            title="Modificar empresa"
                                                                            data-toggle="modal"
                                                                            data-target="#updateModal"
                                                                            data-index="{{$i}}"
                                                                            data-id="{{$empresa->id}}"
                                                                    ><i class="fa fa-folder"></i> Modificar</button>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="{{route('admin.business.users', ['id' => $empresa->id])}}">
                                                                    <button
                                                                        class="btn btn-primary"
                                                                        title="Listado de usuarios"
                                                                    ><i class="fa fa-table"></i> Usuarios</button>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a>
                                                                    <button
                                                                        class="btn btn-info show-cat"
                                                                        data-cat="{{$empresa->client_access_token}}"
                                                                        title="Consultar token del cliente"
                                                                    ><i class="fa fa-bell"></i> Token</button>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a>
                                                                    <button
                                                                        class="btn btn-danger business-down"
                                                                        data-id="{{$empresa->id}}"
                                                                        title="Baja"
                                                                    ><i class="fa fa-trash"></i> Baja</button>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td>Sin registros</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

            <!-- ... Your content goes here ... -->

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="updateModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <form class="form-horizontal" role="form" method="POST" action="{{route('admin.business.update')}}" id="SetUpForm">
                    <input type="hidden" name="id" id="IdEmpresa">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><span id="modal-title"></span></h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Contactos</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" disabled id="Contact">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Cuit en tactica</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control cuit-tactica" disabled>
                            </div>
                            <label class="col-md-2 control-label">Sitio web</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" disabled id="WebSite">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Calificacion</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" disabled id="Calificate">
                            </div>
                        </div>
                        <br>
                        <hr>
                        <br>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Modulos</label>
                            <div class="col-md-10">
                                <select class="selectpicker col-md-12" multiple data-live-search="true" name="modules[]" id="Modules">
                                    @foreach($modules as $module)
                                        <option value="{{$module->key}}">{{$module->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Fecha Expiracion</label>
                            <div class="col-md-4">
                                <input type="text" class="datepicker form-control" name="expire_date" value="12/12/2020">
                            </div>

                            <label class="col-md-2 control-label">Tipo de servicio</label>
                            <div class="col-md-4">
                                <select class="form-control" name="service_type">
                                    @foreach($services as $service)
                                        <option value="{{$service->key}}">{{$service->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-2">

                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Guardar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
    <script>
        lastResults = {!! json_encode($empresas) !!};
    </script>
    <script src="{{ asset('js/business.js') }}"></script>

    <script>
        $('body').on('click', '.openModal', function (e) {
            let i = $(this).data('index');
            let id = $(this).data('id');

            var modules = lastResults[i].business_module.map(x => x['module']['key']);
            console.log(modules);

            $('#updateModal #modal-title').text(lastResults[i].empresa);
            $('#updateModal .cuit-tactica').val(lastResults[i].active_cuit.cuit);
            $('#updateModal #WebSite').val(lastResults[i].sitio_web);
            $('#updateModal #Contact').val(lastResults[i].contacts.join(', '));
            $('#updateModal #Calificate').val(lastResults[i].calificacion);
            $('#updateModal #IdEmpresa').val(id);
            $('#Modules').val(modules);
            $('#Modules').trigger('change');
        });
    </script>
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
@endsection
