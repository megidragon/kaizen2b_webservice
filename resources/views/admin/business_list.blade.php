@extends('admin.layouts.app')

@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">

            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Listado de Empresas en tactica</h1>
                </div>
            </div>

            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Empresas de tactica
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="datatable">
                                    <thead>
                                    <tr>
                                        <th>Fecha Creacion</th>
                                        <th>Empresa</th>
                                        <th>Contacto</th>
                                        <th>Clasificacion</th>
                                        <th>Alta</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if (!empty($empresas_tactica))
                                        @foreach($empresas_tactica as $i => $empresa)
                                            <tr>
                                                <td>{{$empresa->created_at}}</td>
                                                <td>{{$empresa->empresa}}</td>
                                                <td>{{$empresa->contacts ? $empresa->contacts[0] : '-'}}</td>
                                                <td>{{$empresa->qualification ? $empresa->qualification : '-'}}</td>
                                                <td>
                                                    <button
                                                       class="btn btn-primary openModal"
                                                       data-toggle="modal"
                                                       data-target="#detailsModal"
                                                       data-index="{{$i}}"
                                                       data-id="{{$empresa->getOriginal()['tactica_id']}}"
                                                    ><i class="fa fa-plus-circle"></i></button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td>Sin clientes</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

            <!-- ... Your content goes here ... -->

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="detailsModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <form class="form-horizontal" role="form" method="POST" action="" id="SetUpForm">
                    <input type="hidden" name="tactica_id" id="TacticaIdInput">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">"<span id="modal-title"></span>" > Formulario de alta</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Contactos</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" disabled id="Contact">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Cuit en tactica</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control cuit-tactica" disabled>
                            </div>
                            <label class="col-md-2 control-label">Sitio web</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" disabled id="WebSite">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Calificacion</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" disabled id="Calificate">
                            </div>
                        </div>
                        <br>
                        <hr>
                        <br>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Modulos</label>
                            <div class="col-md-10">
                                <select class="selectpicker col-md-12" multiple data-live-search="true" name="modules[]">
                                    @foreach($modules as $module)
                                        <option selected value="{{$module->key}}">{{$module->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Fecha Expiracion</label>
                            <div class="col-md-4">
                                <input type="text" class="datepicker form-control" name="expire_date" value="12/12/2020">
                            </div>

                            <label class="col-md-2 control-label">Tipo de servicio</label>
                            <div class="col-md-4">
                                <select class="form-control" name="service_type">
                                @foreach($services as $service)
                                        <option value="{{$service->key}}">{{$service->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-2">

                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Dar de alta</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
    <script>
        lastResults = {!! json_encode($empresas_tactica) !!};
    </script>

    <script>
        $('body').on('click', '.openModal', function (e) {
            let i = $(this).data('index');
            let id = $(this).data('id');
            $('#detailsModal #modal-title').text(lastResults[i].empresa);
            $('#detailsModal .cuit-tactica').val(lastResults[i].document);
            $('#detailsModal #WebSite').val(lastResults[i].web_site);
            $('#detailsModal #Contact').val(lastResults[i].contacts.join(', '));
            $('#detailsModal #Calificate').val(lastResults[i].qualification);
            $('#detailsModal #TacticaIdInput').val(id);
        });
    </script>
    <script src="{{ asset('js/business.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator !!}
@endsection
