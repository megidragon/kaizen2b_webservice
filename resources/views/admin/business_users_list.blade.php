@extends('admin.layouts.app')

@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">

            <div class="page-header">
                <div class="row">
                    <a href="{{ url()->previous() }}"><button class="btn btn-default"><i class="fa fa-arrow-left"></i> Atras</button></a>
                </div>
                <div class="row">
                    <h1>Listado de usuarios</h1>
                </div>
            </div>

            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Usuarios de la empresa
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="datatable">
                                    <thead>
                                    <tr>
                                        <th width="15%">Fecha alta</th>
                                        <th>Nombre</th>
                                        <th>Email</th>
                                        <th>Estado</th>
                                        <th>Accion</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if (!empty($usersActive) || !empty($usersInactive))
                                        @foreach($usersActive as $i => $user)
                                            <tr>
                                                <td>{{$user->created_at}}</td>
                                                <td>{{$user->name}}</td>
                                                <td>{{$user->email}}</td>
                                                <td>Activo</td>
                                                <td>
                                                    <button
                                                       class="btn btn-danger disable-user"
                                                       title="Inabilitar usuario"
                                                       data-id="<?=$user->id?>"
                                                    ><i class="fa fa-close"></i></button>
                                                </td>
                                            </tr>
                                        @endforeach
                                        @foreach($usersInactive as $i => $user)
                                            <tr>
                                                <td>{{$user->created_at}}</td>
                                                <td>{{$user->name}}</td>
                                                <td>{{$user->email}}</td>
                                                <td>{{$user->status ? 'Inactivo' : 'Bloqueado'}}</td>
                                                <td>
                                                    @if($user->status)
                                                        <button
                                                           class="btn btn-danger disable-user"
                                                           title="Bloquear usuario"
                                                           data-id="<?=$user->id?>"
                                                        ><i class="fa fa-close"></i></button>
                                                    @else
                                                        <button
                                                                class="btn btn-warning disable-user"
                                                                title="Rectivar usuario"
                                                                data-id="<?=$user->id?>"
                                                        ><i class="fa fa-arrow-up"></i></button>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td>Sin usuarios</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

            <!-- ... Your content goes here ... -->

        </div>
    </div>

    <script>
        lastResults = {!! json_encode(array_merge($usersActive->toArray(), $usersInactive->toArray())) !!};
        orderBy = [[3, 'asc']]
    </script>
    <script src="{{ asset('js/business.js') }}"></script>
@endsection
