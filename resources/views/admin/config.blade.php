@extends('admin.layouts.app')

@section('content')
<div id="page-wrapper">
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Configuraciones del sistema</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h3>Configuracion de version de APP global</h3>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
                <input type="text" class="form-control" value="{{$config->getOriginal('global_app_version')}}" readonly>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <button class="btn btn-default openModal" data-target=".modal#versionModal">Actualizar</button>
                <a href="{!! url("/api/app_version_apk/{$config->getOriginal('global_app_version')}") !!}?r={{rand(0, 1000)}}" class="btn btn-info">Descargar version actual</a>
                <button class="btn btn-default openModal" data-target=".modal#versionHistoryModal">Historial de versiones</button>
            </div>

        </div>
        <hr>
        <div class="row">
            <div class="col-md-4">
                <a href="{!! url("/api/download/sync") !!}?r={{rand(0, 1000)}}" class="btn btn-primary">Descargar sincronizador</a>
            </div>
        </div>
        <hr>

        <!-- Modal -->
        <div class="modal fade" id="versionModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <form class="form-horizontal" role="form" method="POST" action="{{route('admin.config.version.update')}}" id="updateVersion" enctype="multipart/form-data">
                        <input type="hidden" name="_method" value="put">
                        <input type="hidden" name="tactica_id" id="TacticaIdInput">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Actualizacion de version</h4>
                        </div>
                        <div class="modal-body">

                            <div class="form-group">
                                <label class="col-md-4 control-label">Ultima version</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" value="{{$config->getOriginal('global_app_version')}}" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Nueva version</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="global_app_version" value="{{$config->getOriginal('global_app_version')}}" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">APK Nueva version</label>
                                <div class="col-md-4">
                                    <input type="file" class="form-control" name="apk_app" accept=".apk" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Nota de version</label>
                                <div class="col-md-4">
                                    <textarea class="form-control" name="app_description" placeholder="Nota.."></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success">Actualizar</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="versionHistoryModal" role="dialog">
            <div class="modal-dialog modal-lg">
                <!-- Modal content-->
                <div class="modal-content">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Descarga</th>
                            <th>Creado</th>
                            <th>Version</th>
                            <th>Notas</th>
                            <th>Usuario</th>
                            <th>Setear version</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($history as $row)
                            <tr style="{{$row->current_version ? 'background-color: #4caa6185;' : null}}">
                                <td>
                                    <a href="{!! url("/api/app_version_apk/{$row->getOriginal('global_app_version')}") !!}?r={{rand(0, 1000)}}" class="btn btn-info hidden-md hidden-lg"><i class="fa fa-download"></i></a>
                                    <a href="{!! url("/api/app_version_apk/{$row->getOriginal('global_app_version')}") !!}?r={{rand(0, 1000)}}" class="btn btn-info hidden-xs hidden-sm">Descargar</a>
                                </td>
                                <td>{{$row->getDate()}}</td>
                                <td>{{$row->getOriginal('global_app_version')}}</td>
                                <td>{{$row->getOriginal('app_description')}}</td>
                                <td>{{$row->user->name}}</td>
                                <td>
                                    <a href="{!! route("admin.config.version.set", ['app_version' => $row->getOriginal('global_app_version')]) !!}?r={{rand(0, 1000)}}" class="btn btn-primary hidden-md hidden-lg"><i class="fa fa-check"></i></a>
                                    <a href="{!! route("admin.config.version.set", ['app_version' => $row->getOriginal('global_app_version')]) !!}?r={{rand(0, 1000)}}" class="btn btn-primary hidden-xs hidden-sm">Establecer version actual</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>

    </div>
</div>

<script>
    $(document).ready(function(){
        $('body').on('click', '.openModal', function (e) {
            $($(this).data('target')).modal();
        });

        @if (\Illuminate\Support\Facades\Session::has('success'))
        Swal.fire({
            type: 'success',
            title: '<h2>Completado</h2>',
            html: '<h4>{{\Illuminate\Support\Facades\Session::get('success')}}</h2>',
            showConfirmButton: true,
        });
        @endif

        @if (\Illuminate\Support\Facades\Session::has('warning'))
        Swal.fire({
            type: 'warning',
            title: '<h2>Error</h2>',
            html: '<h4>{{\Illuminate\Support\Facades\Session::get('warning')}}</h2>',
            showConfirmButton: true,
        });
        @endif

        @if (\Illuminate\Support\Facades\Session::has('error'))
        Swal.fire({
            type: 'error',
            title: '<h2>Se encontro un error en el proceso</h2>',
            html: '{{\Illuminate\Support\Facades\Session::get('error')}}',
            showConfirmButton: true,
        });
        @endif
    });
</script>
@endsection
