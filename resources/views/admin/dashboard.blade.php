@extends('admin.layouts.app')

@section('content')
<div id="page-wrapper">
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Escritorio de trabajo</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-4 col-sm-6" style="height: 209px;">
                <div class="panel panel-info">
                    <div class="panel-body">
                        <h4><i class="fa fa-user"></i> <?=$businessCount?> Empresas activas en el sistema</h4>
                        <span> <?=$businessPercentage?>% de las empresas con sincronizador instalado</span>
                        <span> <?=$acPercentage. '%'?> de las empresas con servicio activo</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <div class="panel panel-info">
                    <div class="panel-heading">Ultima versión de la app</div>
                    <div class="panel-body">
                        <a href="{!! url("/api/app_version_apk/{$config->getOriginal('global_app_version')}") !!}?r={{rand(0, 1000)}}" class="btn btn-info">Descargar version actual</a>
                    </div>
                </div>
            </div>
        </div>
        <hr>

        <div class="row">
            <div class="col-md-4">
                <a href="{{ route('admin.business') }}">
                    <div class="panel panel-info">
                        <div class="panel-heading">Clientes en tactica</div>
                        <div class="panel-body">Listado de empresas cargadas en la base de datos de tactica.<br>Permite dar de alta empresas en el sistema</div>
                    </div>
                </a>
            </div>
            <div class="col-md-4">
                <a href="{{ route('admin.business.active') }}">
                    <div class="panel panel-info">
                        <div class="panel-heading">Empresas activas</div>
                        <div class="panel-body">Clientes dados de alta en el sistema</div>
                    </div>
                </a>
            </div>
            <div class="col-md-4">
                <a href="{{ route('admin.config') }}">
                    <div class="panel panel-info">
                        <div class="panel-heading">Configuracion del sistema</div>
                        <div class="panel-body">Configuraciones generales del sistema</div>
                    </div>
                </a>
            </div>
        </div>

    </div>
</div>
@endsection
