<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Kaizen2B') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script type="application/javascript">
        function readToken(){
            let token = null;
            @if (!empty(Session::get('token')))
                token = '{!! Session::get('token') !!}';
            @endif

            return token;
        }

        const BASE_URL = '{{url('/')}}/';
        const BASE_URL_WS = BASE_URL+'api/';
    </script>
    <script src="{{ asset('js/dataTables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/dataTables/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/requests.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Bootstrap Select CSS -->
    <link href="{{ asset('css/bootstrap-select.css') }}" rel="stylesheet">

    <!-- Bootstrap datepicker CSS -->
    <link href="{{ asset('css/bootstrap-datepicker.css') }}" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="{{ asset('css/metisMenu.min.css') }}" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="{{ asset('css/timeline.css') }}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ asset('css/startmin.css') }}" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="{{ asset('css/morris.css') }}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">

    <link href="{{ asset('css/styles.css') }}" rel="stylesheet" type="text/css">
</head>
<body>
<div id="wrapper">
    @if ($errors->any())
        {{ implode('', $errors->all('<div>:message</div>')) }}
    @endif
    <!-- Navigation -->
    @auth
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <a class="navbar-brand" href="{{route('admin.dashboard')}}">Kaizen2b - Backend</a>
        <!-- Top Navigation: Right Menu -->
        <ul class="nav navbar-right navbar-top-links">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i> {{auth()->user()->name}} <b class="caret"></b>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="{{route('logout')}}"><i class="fa fa-sign-out fa-fw"></i> Salir</a>
                    </li>
                </ul>
            </li>
        </ul>

        <!-- Sidebar -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">

                <ul class="nav" id="side-menu">
                    <li class="sidebar-search">
                        <img src="{{url('/images/kzn_logo.png')}}" alt="Kaizen2B"/>
                    </li>
                    <li>
                        <h3 style="margin-left: 10px;">ADMINISTRACION</h3>
                    </li>
                    <li>
                        <a href="{{ route('admin.dashboard') }}"><i class="fa fa-home fa-fw"></i> Escritorio</a>
                    </li>
                    <li>
                        <a href="{{ route('dashboard') }}"><i class="fa fa-mobile fa-fw"></i> Web APP</a>
                    </li>
                    <li>
                        <a href="{{ route('admin.business') }}"><i class="fa fa-clipboard fa-fw"></i> Clientes en Tactica</a>
                    </li>
                    <li>
                        <a href="{{ route('admin.business.active') }}"><i class="fa fa-building fa-fw"></i> Empresas Activas</a>
                    </li>

                    @if (auth()->user()->hasRole(\App\Role\UserRole::ROLE_ADMIN))
                        <li>
                            <a href="{{ route('admin.users') }}"><i class="fa fa-users"></i> Usuarios administradores</a>
                        </li>
                        <li>
                            <a href="{{ route('admin.config') }}"><i class="fa fa-cog"></i> Configuraciones del sistema</a>
                        </li>
                        <li>
                            <a href="{{ route('admin.audit') }}"><i class="fa fa-list"></i> Auditoria</a>
                        </li>
                     @endif
                </ul>

            </div>
        </div>
    </nav>
    @endauth

    @yield('content')


    </div>
    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('js/metisMenu.min.js') }}"></script>
    <script src="{{ asset('js/startmin.js') }}"></script>
    <script src="{{ asset('js/sweetalert2.js') }}"></script>
    <script src="{{ asset('js/bootstrap-notify.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.datepicker').datepicker();
        });
        @if (\Illuminate\Support\Facades\Session::has('success'))
            $.notify({
                // options
                message: '{{\Illuminate\Support\Facades\Session::get('success')}}'
            },{
                // settings
                type: 'success'
            });
        @endif

        @if (\Illuminate\Support\Facades\Session::has('error'))
            $.notify({
                // options
                message: '{{\Illuminate\Support\Facades\Session::get('error')}}'
            },{
                // settings
                type: 'danger'
            });
        @endif
    </script>
</body>
</html>
