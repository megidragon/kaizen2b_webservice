@extends('admin.layouts.app')

@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">

            <div class="page-header">
                <div class="row">
                    <a href="{{ url()->previous() }}"><button class="btn btn-default"><i class="fa fa-arrow-left"></i> Atras</button></a>
                </div>
                <div class="row">
                    <h1>{{ !empty($user) ? "Modificar ususario {$user->name}" : 'Alta de nuevo usuario' }}</h1>
                </div>
            </div>

            <!-- /.row -->
            <div class="row">
                <div class="col-md-8">
                    <form method="post" action="{!! empty($user) ? route('admin.users.store') : route('admin.users.update', ['id' => $user->id])!!}">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="NameInput">Nombre</label>
                                    <input type="text" class="form-control" id="NameInput" name="name" placeholder="Nombre" value="{!! !empty($user) ? $user->name : '' !!}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="EmailInput">Email</label>
                                    <input type="email" class="form-control" id="EmailInput" name="email" placeholder="Email" value="{!! !empty($user) ? $user->email : '' !!}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="PasswordInput">Contraseña</label>
                                    <input type="password" class="form-control" name="password" placeholder="Contraseña" autocomplete="new-password">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="RePasswordInput">Repetir contraseña</label>
                                    <input type="password" class="form-control" id="RePasswordInput" name="password_confirmation" placeholder="Confirme contraseña">
                                </div>
                            </div>
                        </div>

                        @if(!empty($user))
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="StatusInput" name="status" value="1" {{empty($user) || $user->status ? 'checked' : ''}}>
                                <label class="form-check-label" for="StatusInput">Usuario activo</label>
                            </div>
                        @endif
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </form>
                </div>
            </div>
            <!-- /.row -->

            <!-- ... Your content goes here ... -->

        </div>
    </div>
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator !!}
@endsection
