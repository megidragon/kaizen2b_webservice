@extends('admin.layouts.app')

@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">

            <div class="page-header">
                <div class="row">
                    <a href="{{ url()->previous() }}"><button class="btn btn-default"><i class="fa fa-arrow-left"></i> Atras</button></a>
                </div>
                <div class="row">
                    <h1>Ver usuario "{{$user->name}}"</h1>
                </div>
            </div>


            <!-- /.row -->
            <div class="row">
                <div class="col-md-8">
                    <form>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="NameInput">Nombre</label>
                                    <input readonly type="text" class="form-control" id="NameInput" name="name" placeholder="Nombre" value="{!! !empty($user) ? $user->name : '' !!}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="EmailInput">Email</label>
                                    <input readonly type="email" class="form-control" id="EmailInput" name="email" placeholder="Email" value="{!! !empty($user) ? $user->email : '' !!}">
                                </div>
                            </div>
                        </div>

                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="StatusInput" {{empty($user) || $user->status ? 'checked' : ''}}>
                            <label class="form-check-label" for="StatusInput">Usuario activo</label>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.row -->

            <!-- ... Your content goes here ... -->

        </div>
    </div>
@endsection
