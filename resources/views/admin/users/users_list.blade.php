@extends('admin.layouts.app')

@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">

            <div class="page-header">
                <div class="row">
                    <h1>Listado de usuarios administradores</h1>
                </div>
                <div class="row">
                    <a href="{{ route('admin.users.store.view') }}"><button class="btn btn-primary"><i class="fa fa-plus"></i> Nuevo usuario</button></a>
                </div>
            </div>

            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Usuarios
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive" style="min-height: 500px;">
                                <table class="table table-striped table-bordered table-hover" id="datatable">
                                    <thead>
                                    <tr>
                                        <th width="15%">Fecha creacion</th>
                                        <th>Nombre</th>
                                        <th>Email</th>
                                        <th>Status</th>
                                        <th width="15%">Acciones</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if (!empty($users))
                                        @foreach($users as $i => $user)
                                            <tr>
                                                <td>{{$user->created_at}}</td>
                                                <td>{{$user->name}}</td>
                                                <td>{{$user->email}}</td>
                                                <td>{{$user->status ? 'Activo' : 'Inactivo'}}</td>
                                                <td>
                                                    <div class="dropdown">
                                                        <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Acciones
                                                            <span class="caret"></span></button>
                                                        <ul class="dropdown-menu">
                                                            <li>
                                                                <a href="{!! route('admin.users.show.view', ['id' => $user->id]) !!}">
                                                                    <button class="btn btn-info" title="Ver"><i class="fa fa-user"></i> Ver</button>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="{!! route('admin.users.update.view', ['id' => $user->id]) !!}">
                                                                    <button class="btn btn-primary" title="Modificar"><i class="fa fa-cog"></i> Modificar</button>
                                                                </a>
                                                            </li>
                                                            <li><a><button class="btn btn-danger delete-user" data-id="{{$user->id}}" title="Eliminar"><i class="fa fa-trash"></i> Eliminar</button></a></li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td>Sin usuarios</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

            <!-- ... Your content goes here ... -->

        </div>
    </div>
    <script>
        lastResults = {!! json_encode($users) !!};
    </script>
    <script src="{{ asset('js/users.js') }}"></script>
@endsection
