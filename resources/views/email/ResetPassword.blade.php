<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Email</title>
</head>
<body>
    <h1>Reinicio de contraseña Kaizen2b</h1>
    <br>
    <p>Ingrese al siguiente enlace para limpiar su contraseña <a href="{{route('forgot-password.reset', $token)}}">{{route('forgot-password.reset', $token)}}</a></p>
    <p>El enlace solo estara disponible en los proximos 15 minutos.</p>
</body>
</html>