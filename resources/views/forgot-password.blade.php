<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>{{ env('APP_NAME') }}</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{!! asset('plugins/bootstrap/css/bootstrap.css') !!}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{!! asset('plugins/node-waves/waves.css') !!}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{!! asset('plugins/animate-css/animate.css') !!}" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="{!! asset('plugins/morrisjs/morris.css') !!}" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{!! asset('css/style.css') !!}" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{!! asset('css/themes/all-themes.css" rel="stylesheet') !!}" />

    {!! RecaptchaV3::initJs() !!}
</head>

<body class="fp-page">

<div class="fp-box">
    <div class="logo">
        <a href="javascript:void(0);">Kaizen2b<b> Web APP</b></a>
    </div>
    <div class="card">
        <div class="body">
            <form id="forgot_password" action="{{route('forgot-password.send')}}" method="POST">
                <div class="msg">
                    Ingrese su email para enviarle un link de confirmacion.
                </div>
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">build</i>
                        </span>
                    <div class="form-line">
                        <input type="text" class="form-control" name="cuit" placeholder="Cuit empresa" required autofocus>
                    </div>
                </div>
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                    <div class="form-line">
                        <input type="email" class="form-control" name="email" placeholder="Email" required autofocus>
                    </div>
                </div>

                <button class="btn btn-block btn-lg bg-pink waves-effect" type="submit">Reiniciar mi contraseña</button>

                <div class="row m-t-20 m-b--5 align-center">
                    <a href="{{route('login')}}">Login</a>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Jquery Core Js -->
<script src="{!! asset('plugins/jquery/jquery.min.js') !!}"></script>

<!-- Bootstrap Core Js -->
<script src="{!! asset('plugins/bootstrap/js/bootstrap.js') !!}"></script>

<!-- Select Plugin Js -->
<script src="{!! asset('plugins/bootstrap-select/js/bootstrap-select.js') !!}"></script>

<!-- Slimscroll Plugin Js -->
<script src="{!! asset('plugins/jquery-slimscroll/jquery.slimscroll.js') !!}"></script>

<!-- Waves Effect Plugin Js -->
<script src="{!! asset('plugins/node-waves/waves.js') !!}"></script>

<!-- Jquery CountTo Plugin Js -->
<script src="{!! asset('plugins/jquery-validation/jquery.validate.js') !!}"></script>

<!-- Custom Js -->
<script src="{!! asset('js/admin.js') !!}"></script>
<script src="{!! asset('js/pages/examples/forgot-password.js') !!}"></script>

<!-- Demo Js -->
<script src="{!! asset('js/demo.js') !!}"></script>
<script src="{{ asset('js/sweetalert2.js') }}"></script>
<script src="{{ asset('js/bootstrap-notify.min.js') }}"></script>

<script>
    @if (\Illuminate\Support\Facades\Session::has('success'))
    $.notify({
        // options
        message: '{{\Illuminate\Support\Facades\Session::get('success')}}'
    },{
        // settings
        type: 'success'
    });
    @endif

    @if (\Illuminate\Support\Facades\Session::has('error'))
    $.notify({
        // options
        message: '{{\Illuminate\Support\Facades\Session::get('error')}}'
    },{
        // settings
        type: 'danger'
    });
    @endif
</script>

</body>

</html>
