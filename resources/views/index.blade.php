<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>{{env('APP_NAME')}}</title>
    <!-- Favicon-->
    {{--<link rel="icon" href="favicon.ico" type="image/x-icon">--}}

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{!! asset('plugins/bootstrap/css/bootstrap.css') !!}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{!! asset('plugins/node-waves/waves.css') !!}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{!! asset('plugins/animate-css/animate.css') !!}" rel="stylesheet" />

    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="{!! asset('plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') !!}" rel="stylesheet" />

    <!-- Bootstrap DatePicker Css -->
    <link href="{!! asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.css') !!}" rel="stylesheet" />

    <!-- Wait Me Css -->
    <link href="{!! asset('plugins/waitme/waitMe.css') !!}" rel="stylesheet" />

    <!-- Bootstrap Select Css -->
    <link href="{!! asset('plugins/bootstrap-select/css/bootstrap-select.css') !!}" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="{!! asset('plugins/morrisjs/morris.css') !!}" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{!! asset('css/style.css') !!}" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{!! asset('css/themes/all-themes.css" rel="stylesheet') !!}" />

    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<body class="theme-black">

<div id="app">
    <app-component></app-component>
</div>

<script>
    @if (auth()->check())
        userData = {!! json_encode(auth()->user()->toArray(), true) !!};
    @else
        userData = null;
     @endif
</script>

<!-- Main app -->
<script src="{!! asset('js/app.js') !!}"></script>

<!-- Jquery Core Js -->
<script src="{!! asset('plugins/jquery/jquery.min.js') !!}"></script>

<script src="{!! asset('/plugins/jquery-datatable/jquery.dataTables.js') !!}"></script>
<script src="{!! asset('/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') !!}"></script>
<script src="{!! asset('/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') !!}"></script>
<script src="{!! asset('/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') !!}"></script>
<script src="{!! asset('/plugins/jquery-datatable/extensions/export/jszip.min.js') !!}"></script>
<script src="{!! asset('/plugins/jquery-datatable/extensions/export/pdfmake.min.js') !!}"></script>
<script src="{!! asset('/plugins/jquery-datatable/extensions/export/vfs_fonts.js') !!}"></script>
<script src="{!! asset('/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') !!}"></script>
<script src="{!! asset('/plugins/jquery-datatable/extensions/export/buttons.print.min.js') !!}"></script>

<!-- Bootstrap Core Js -->
<script src="{!! asset('plugins/bootstrap/js/bootstrap.js') !!}"></script>

<!-- Select Plugin Js -->
<script src="{!! asset('plugins/bootstrap-select/js/bootstrap-select.js') !!}"></script>

<!-- Slimscroll Plugin Js -->
<script src="{!! asset('plugins/jquery-slimscroll/jquery.slimscroll.js') !!}"></script>

<!-- Waves Effect Plugin Js -->
<script src="{!! asset('plugins/node-waves/waves.js') !!}"></script>

<!-- Jquery CountTo Plugin Js -->
<script src="{!! asset('plugins/jquery-countto/jquery.countTo.js') !!}"></script>

<!-- Morris Plugin Js -->
<script src="{!! asset('plugins/raphael/raphael.min.js') !!}"></script>
<script src="{!! asset('plugins/morrisjs/morris.js') !!}"></script>

<!-- ChartJs -->
<script src="{!! asset('plugins/chartjs/Chart.bundle.js') !!}"></script>

<!-- Flot Charts Plugin Js -->
<script src="{!! asset('plugins/flot-charts/jquery.flot.js') !!}"></script>
<script src="{!! asset('plugins/flot-charts/jquery.flot.resize.js') !!}"></script>
<script src="{!! asset('plugins/flot-charts/jquery.flot.pie.js') !!}"></script>
<script src="{!! asset('plugins/flot-charts/jquery.flot.categories.js') !!}"></script>
<script src="{!! asset('plugins/flot-charts/jquery.flot.time.js') !!}"></script>

<!-- Sparkline Chart Plugin Js -->
<script src="{!! asset('plugins/jquery-sparkline/jquery.sparkline.js') !!}"></script>

<!-- Custom Js -->
<script src="{!! asset('js/admin.js') !!}"></script>

<!-- Demo Js -->
<script src="{!! asset('js/demo.js') !!}"></script>

</body>

</html>
