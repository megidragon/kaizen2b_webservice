<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>{{ env('APP_NAME') }}</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{!! asset('plugins/bootstrap/css/bootstrap.css') !!}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{!! asset('plugins/node-waves/waves.css') !!}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{!! asset('plugins/animate-css/animate.css') !!}" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="{!! asset('plugins/morrisjs/morris.css') !!}" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{!! asset('css/style.css') !!}" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{!! asset('css/themes/all-themes.css" rel="stylesheet') !!}" />

    {!! RecaptchaV3::initJs() !!}
</head>

<body class="login-page">

<div class="login-box">
    <div class="logo">
        <a href="javascript:void(0);">Kaizen2b<b> Web APP</b></a>
    </div>
    <div class="card">
        <div class="body">
            <form id="login" method="POST">
                <div class="msg">Ingrese sus credenciales</div>
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">build</i>
                        </span>
                    <div class="form-line">
                        <input type="text" class="form-control" name="cuit" placeholder="Cuit" required autofocus>
                    </div>
                </div>
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                    <div class="form-line">
                        <input type="email" class="form-control" name="email" placeholder="Email" required>
                    </div>
                </div>
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                    <div class="form-line">
                        <input type="password" class="form-control" name="password" placeholder="Password">
                    </div>
                </div>

                {!! RecaptchaV3::field('login') !!}

                <div class="row">
                    <div class="col-xs-8 p-t-5">
                        <input type="checkbox" name="rememberme" id="rememberme" class="filled-in chk-col-pink">
                        <label for="rememberme">Mantener sesion</label>
                    </div>
                    <div class="col-xs-4">
                        <button class="btn btn-block bg-pink waves-effect" type="submit">Log In</button>
                    </div>
                </div>
                <div class="row m-t-15 m-b--20">
                    <div class="col-xs-6">
                        <a href="{{route('forgot-password')}}">¿Olvido su contraseña?</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="NewUserModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Dar de alta usuario</h4>
            </div>
            <div class="modal-body">
                <h3>Ingrese una contraseña para dar de alta el usuario</h3>
                <form role="form" id="NewPasswordForm">
                    <div class="form-group">
                        <input class="form-control" placeholder="Nueva contraseña" name="password" required type="password">
                    </div>
                    <div class="form-group">
                        <input class="form-control" placeholder="Repita contraseña" name="password2" required type="password">
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Enviar" class="btn btn-success">
                    </div>
                </form>
            </div>
            {{--<div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>--}}
        </div>

    </div>
</div>

<script>
    @if (auth()->check())
        userData = {!! json_encode(auth()->user()->toArray(), true) !!};
    @else
        userData = null;
    @endif
</script>

<!-- Jquery Core Js -->
<script src="{!! asset('plugins/jquery/jquery.min.js') !!}"></script>

<!-- Bootstrap Core Js -->
<script src="{!! asset('plugins/bootstrap/js/bootstrap.js') !!}"></script>

<!-- Select Plugin Js -->
<script src="{!! asset('plugins/bootstrap-select/js/bootstrap-select.js') !!}"></script>

<!-- Slimscroll Plugin Js -->
<script src="{!! asset('plugins/jquery-slimscroll/jquery.slimscroll.js') !!}"></script>

<!-- Waves Effect Plugin Js -->
<script src="{!! asset('plugins/node-waves/waves.js') !!}"></script>

<!-- Jquery CountTo Plugin Js -->
<script src="{!! asset('plugins/jquery-countto/jquery.countTo.js') !!}"></script>

<!-- Custom Js -->
<script src="{!! asset('js/admin.js') !!}"></script>

<!-- Demo Js -->
<script src="{!! asset('js/demo.js') !!}"></script>

<script type="application/javascript">
    function readToken(){
        let token = null;
        @if (!empty(Session::get('token')))
            token = '{!! Session::get('token') !!}';
        @endif

            return token;
    }

    const BASE_URL = '{{url('/')}}/';
    const BASE_URL_WS = BASE_URL+'api/';
</script>
<script src="{{ asset('js/sweetalert2.js') }}"></script>
<script src="{{ asset('js/requests.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>

<script type="application/javascript">
    reCAPTCHA_site_key = '{!! env('RECAPTCHAV3_SITEKEY') !!}';
    new_user_url = '';
    $(document).ready(function () {
        $('#login').on('submit', function(event){
            event.preventDefault();
            if (!$(this).find('input[name=remember]').val()){
                rememberMe = 15*60;
            }else{
                rememberMe = null;
            }
            loginWs($(this).find('input[name=cuit]').val(), $(this).find('input[name=email]').val(), $(this).find('input[name=password]').val(), $(this).find('input[name=g-recaptcha-response]').val(), rememberMe);
        });

        $('#NewPasswordForm').submit(function (e) {
            e.preventDefault();
            let pass = $(this).find('input[name=password]').val();
            let pass2 = $(this).find('input[name=password2]').val();
            if (pass.length > 0 && pass === pass2){
                if (!new_user_url.length){
                    alert('Error fatal, url no encontrada');
                }
                $.post(new_user_url, {
                    'new_password': pass
                },function (res) {
                    if(res.success){
                        $('button.close').trigger('click');
                        alert(res.msg);
                    }else{
                        alert('Error fatal, usuario no creado');
                    }
                }).error(function (err) {
                    alert('Error fatal desconocido');
                });
            }
            return false;
        });


    });
</script>

<script src="{{ asset('js/bootstrap-notify.min.js') }}"></script>

<script>
    @if (\Illuminate\Support\Facades\Session::has('success'))
    $.notify({
        // options
        message: '{{\Illuminate\Support\Facades\Session::get('success')}}'
    },{
        // settings
        type: 'success'
    });
    @endif

    @if (\Illuminate\Support\Facades\Session::has('error'))
    $.notify({
        // options
        message: '{{\Illuminate\Support\Facades\Session::get('error')}}'
    },{
        // settings
        type: 'danger'
    });
    @endif
</script>

</body>

</html>
