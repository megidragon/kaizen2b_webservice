<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([
    'middleware' => ['api', 'cors'],
], function ($router) {
    Route::post('login', 'PassportController@login');
    Route::post('register', 'PassportController@register');
    Route::post('new-user/{cuit}/{email}', 'PassportController@newUser');

    Route::get('app_version_apk/{version}', 'HomeController@downloadApk');
    Route::get('download/sync', 'HomeController@downloadSync');

    Route::middleware(['cors', 'auth:api', 'check_user_role:' . \App\Role\UserRole::ROLE_CLIENT])->group(function () {
        Route::get('logout', 'PassportController@logout');
        Route::get('user', 'PassportController@details');

        Route::post('log', 'HomeController@logInsert');

        Route::get('activities', 'API\ActivitiesController@index');
        Route::get('activities/{id}', 'API\ActivitiesController@show');
        Route::patch('activities', 'API\ActivitiesController@verifyHash');
    });

    Route::middleware(['cors', 'auth:api', 'check_user_role:' . \App\Role\UserRole::ROLE_CLIENT_PRO])->group(function () {
        Route::post('activities', 'API\ActivitiesController@store');
        Route::put('activities/{id}', 'API\ActivitiesController@update');
        Route::delete('activities/{id}', 'API\ActivitiesController@destroy');
    });

    Route::middleware(['cors', 'auth.business'])->group(function () {
        Route::get('sync', 'SyncController@info');
        Route::get('service', 'SyncController@serviceStatus');
        Route::post('sync', 'SyncController@sync');
    });
});
