<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*Route::get('/admin', function () {
    return view('welcome2');
})->middleware('check_user_role:' . \App\Role\UserRole::ROLE_CLIENT_PRO);*/

#Auth::routes();

Route::get('/login', 'LoginController@login')->name('login');
Route::post('/login', 'LoginController@loginWs')->name('login_ws');

Route::get('/forgot-password', 'LoginController@forgotPassword')->name('forgot-password');
Route::post('/forgot-password', 'LoginController@sendForgotPasswordMail')->name('forgot-password.send');
Route::get('/reset-password/{token}', 'LoginController@resetPassword')->name('forgot-password.reset');


Route::middleware(['auth', 'check_user_role:' . \App\Role\UserRole::ROLE_CLIENT])->group(function () {

    /* Web app routes */

    Route::get('/', 'HomeController@index')->name('dashboard');
    Route::get('/activities', 'HomeController@index')->name('activities');
    Route::get('/profile', 'HomeController@index')->name('profile');

    /* END Web app routes */





    //Route::get('/activities/{id}', 'HomeController@activities')->name('activities.show');
    Route::get('/logout', 'LoginController@logOut')->name('logout');

    Route::prefix('kzn-admin')->middleware(['check_user_role:' . \App\Role\UserRole::ROLE_SUB_ADMIN])->group(function () {
        Route::get('/', 'Admin\DashboardController@index')->name('admin.dashboard');

        // Auditoria
        Route::get('/audit', 'Admin\AuditoryController@index')->name('admin.audit');

        // Configuraciones
        Route::get('/config', 'Admin\ConfigurationController@index')->name('admin.config');
        Route::put('/config/version/update', 'Admin\ConfigurationController@setAppVersion')->name('admin.config.version.update');
        Route::get('/config/version/set/{app_version}', 'Admin\ConfigurationController@changeCurrentVersion')->name('admin.config.version.set');

        // Business
        Route::get('/business', 'Admin\BusinessController@index')->name('admin.business');
        Route::get('/business/{id}/users', 'Admin\BusinessController@users')->name('admin.business.users');
        Route::post('/business', 'Admin\BusinessController@create')->name('admin.business');
        Route::post('/business/update', 'Admin\BusinessController@update')->name('admin.business.update');

        // Active business
        Route::get('/active-business', 'Admin\BusinessController@active')->name('admin.business.active');
        Route::delete('/active-business/delete/{id}', 'Admin\BusinessController@down')->name('admin.business.down');
        Route::delete('/active-business/users/disable/{id}', 'Admin\BusinessController@userDisable')->name('admin.business.users.disable');


        Route::middleware(['check_user_role:' . \App\Role\UserRole::ROLE_ADMIN])->group(function () {
            // Admin users
            // Views
            Route::get('/users', 'Admin\UsersController@index')->name('admin.users');
            Route::get('/users/new', 'Admin\UsersController@store')->name('admin.users.store.view');
            Route::get('/users/{id}/show', 'Admin\UsersController@show')->name('admin.users.show.view');
            Route::get('/users/{id}/update', 'Admin\UsersController@update')->name('admin.users.update.view');

            // Requests
            Route::post('/users', 'Admin\UsersController@store')->name('admin.users.store');
            Route::post('/users/{id}/update', 'Admin\UsersController@update')->name('admin.users.update');
            Route::delete('/users/{id}', 'Admin\UsersController@delete')->name('admin.users.delete');
        });
    });
});
